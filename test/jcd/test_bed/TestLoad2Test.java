/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.shape.Line;
import jcd.data.DataManager;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import jcd.file.FileManager;
import static jcd.test_bed.TestLoad.FILE_PATH;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author haley.he
 */
public class TestLoad2Test {
    DataManager dataM;
    
    public TestLoad2Test() {
        dataM = new DataManager();
        FileManager fm = new FileManager();
        try {
            fm.loadData(dataM, TestLoad.FILE_PATH2);
        } catch (IOException ex) {
            System.out.println("FAIL");
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }


    /**
     * Test of printRelationInfo method, of class TestLoad.
     */
    @Test
    public void testPrintRelationInfo() {
        System.out.println("printRelationInfo");
        ArrayList<ArrayList<Node>> relations = dataM.getRelationships();
        TestLoad.printRelationInfo(relations);
        assertEquals(relations.size(),4);
        ArrayList<Node> line1 = relations.get(0);
        
        Line l1 = (Line) line1.get(0);
        assertTrue(l1.contains(10,200));
        assertTrue(l1.contains(105,200));
        
        Line l2 = (Line) line1.get(2);
        assertTrue(l2.contains(105,200));
        assertTrue(l2.contains(200,200));
        
                ArrayList<Node> line2 = relations.get(2);
                        Line l3 = (Line) line2.get(0);
                        assertTrue(l3.contains(123,234));
    }

    /**
     * Test of printClassInfo method, of class TestLoad.
     */
    @Test
    public void testPrintClassInfo() {
        System.out.println("printClassInfo");
        ArrayList<UMLObject> umlArray = dataM.getUMLObjs();
        TestLoad.printClassInfo(umlArray);;
        UMLObject aClass = umlArray.get(0);
        assertTrue(aClass.getIsAbstract());
        assertFalse(aClass.getIsInterface());
        assertTrue(aClass.getX()==10.0);
        assertTrue(aClass.getY()==10.0);
        
        HashMap<String,Method> acmMap = aClass.getMethodMap();
        HashMap<String,String> dohwMap = acmMap.get("doHw").getArgMap();
        assertEquals(dohwMap.get("cry"),"int");
        assertTrue(dohwMap.containsKey("cry"));
        assertTrue(acmMap.get("start").getIsAbstract()); //************************************
        
        UMLObject pause = umlArray.get(2);
        HashMap<String,Variable> varMap = pause.getVarMap();
        assertEquals(varMap.get("smile").getName(),"smile");
        assertEquals(varMap.get("app").getType(),"ThreadExample");
        
        HashMap<String,Method> mMap = pause.getMethodMap();
        HashMap<String,String> atMap = mMap.get("PauseHandler").getArgMap();
        assertEquals(atMap.get("initApp"),"ThreadExample");
        assertTrue(atMap.containsKey("initApp"));
    }

    /**
     * Test of printTreeInfo method, of class TestLoad.
     */
    @Test
    public void testPrintTreeInfo() {
        System.out.println("printTreeInfo");
        TreeItem root = dataM.getProject();
        TestLoad.printTreeInfo(root);
        TreeItem defP = (TreeItem) root.getChildren().get(0);
        TreeItem aItem = (TreeItem) defP.getChildren().get(1);
        UMLObject aClass = (UMLObject) aItem.getValue();
        assertEquals(aClass.getName(),"AbstractClass");
    }
    
}

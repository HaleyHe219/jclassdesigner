/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.shape.Line;
import jcd.data.DataManager;
import jcd.data.JavaPackage;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import jcd.file.FileManager;
import static jcd.test_bed.TestLoad.FILE_PATH;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author haley.he
 */
public class TestLoadTest {
    DataManager dataM;
    
    public TestLoadTest() {
        dataM = new DataManager();
        FileManager fm = new FileManager();
        try {
            fm.loadData(dataM, FILE_PATH);
        } catch (IOException ex) {
            System.out.println("FAIL");
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }


    /**
     * Test of printRelationInfo method, of class TestLoad.
     */
    @Test
    public void testPrintRelationInfo() {
        System.out.println("printRelationInfo");
        ArrayList<ArrayList<Node>> relations = dataM.getRelationships();
        TestLoad.printRelationInfo(relations);
        assertEquals(relations.size(),3);
        ArrayList<Node> line1 = relations.get(0);
        
        Line l1 = (Line) line1.get(0);
        assertTrue(l1.contains(10,100));
        assertTrue(l1.contains(55,100));
        
        Line l2 = (Line) line1.get(2);
        assertTrue(l2.contains(55,100));
        assertTrue(l2.contains(100,100));
        
    }

    /**
     * Test of printClassInfo method, of class TestLoad.
     */
    @Test
    public void testPrintClassInfo() {
        System.out.println("printClassInfo");
        ArrayList<UMLObject> umlArray = dataM.getUMLObjs();
        TestLoad.printClassInfo(umlArray);
        UMLObject threadEx = umlArray.get(0);
        assertEquals(threadEx.getName(),"ThreadExample"); 
        
        HashMap<String,Variable> varMap = threadEx.getVarMap();
        assertTrue(varMap.get("START_TEXT").getIsStatic());
        assertEquals(varMap.get("pauseButton").getName(),"pauseButton");
        assertEquals(varMap.get("scrollPane").getType(),"ScrollPane");
        
        HashMap<String,Method> mMap = threadEx.getMethodMap();
        HashMap<String,String> atMap = mMap.get("appendText").getArgMap();
        assertEquals(atMap.get("textToAppend"),"String");
        assertTrue(atMap.containsKey("textToAppend"));
        
        UMLObject start = umlArray.get(2);
        assertTrue(start.getY()==700);
        assertTrue(start.getX()==600);
        HashMap<String,Method> startmMap = start.getMethodMap();
        HashMap<String,String> hMap = startmMap.get("handle").getArgMap();
        assertEquals(hMap.get("event"),"Event");
        assertFalse(hMap.containsKey("eventsz"));
    }

    /**
     * Test of printTreeInfo method, of class TestLoad.
     */
    @Test
    public void testPrintTreeInfo() {
        System.out.println("printTreeInfo");
        TreeItem root = dataM.getProject();
        TestLoad.printTreeInfo(root);
        TreeItem javafx = (TreeItem) root.getChildren().get(2);
        JavaPackage javafxData = (JavaPackage) javafx.getValue();
        assertEquals(javafxData.getPackageName(),"javafx");
    }
    
}

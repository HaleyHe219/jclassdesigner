/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import jcd.data.DataManager;
import jcd.data.JavaPackage;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import jcd.file.FileManager;
import static jcd.test_bed.TestLoad.FILE_PATH;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author haley.he
 */
public class TestLoad3Test {
    DataManager dataM;
    
    public TestLoad3Test() {
                dataM = new DataManager();
        FileManager fm = new FileManager();
        try {
            fm.loadData(dataM, TestLoad.FILE_PATH3);
        } catch (IOException ex) {
            System.out.println("FAIL");
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }


    /**
     * Test of printRelationInfo method, of class TestLoad.
     */
    @Test
    public void testPrintRelationInfo() {
        System.out.println("printRelationInfo");
        ArrayList<ArrayList<Node>> relations = dataM.getRelationships();
        TestLoad.printRelationInfo(relations);
        ArrayList<Node> line1 = relations.get(0);
        assertTrue(line1.get(line1.size()-1) instanceof Rectangle);
        assertTrue(relations.get(1).get(line1.size()-1) instanceof Polyline);
        assertTrue(relations.get(2).get(line1.size()-1) instanceof Rectangle);
        Line l1 = (Line) line1.get(0);
        assertTrue(l1.contains(400,300));
        assertTrue(l1.contains(250,200));
        Line l2 = (Line) line1.get(2);
        assertTrue(l2.contains(100,100));
    }
        
        

    /**
     * Test of printClassInfo method, of class TestLoad.
     */
    @Test
    public void testPrintClassInfo() {
        System.out.println("printClassInfo");
        ArrayList<UMLObject> umlArray = dataM.getUMLObjs();
        TestLoad.printClassInfo(umlArray);
        UMLObject start = umlArray.get(4);
         HashMap<String,Variable> varMap = start.getVarMap();
        assertTrue(varMap.containsKey("app"));
        assertEquals(varMap.get("app").getType(),"ThreadExample");
        assertTrue(start.getX()==600);
        assertTrue(start.getY()==700);
    }

    /**
     * Test of printTreeInfo method, of class TestLoad.
     */
    @Test
    public void testPrintTreeInfo() {
        System.out.println("printTreeInfo");
        TreeItem root = dataM.getProject();
        TestLoad.printTreeInfo(root);
        assertTrue(root.getChildren().size() == 3);
        TreeItem def= (TreeItem) root.getChildren().get(0);
        TreeItem dragItem = (TreeItem) def.getChildren().get(1);
        UMLObject draggable = (UMLObject) dragItem.getValue();
        assertEquals(draggable.getName(),"DragInterface");
        assertTrue(draggable.getIsInterface());
        
        HashMap<String,Variable> vmap = draggable.getVarMap();
        assertTrue(vmap.size()==0);
        
        HashMap<String,Method> mmap = draggable.getMethodMap();
        Method drag = mmap.get("drag");
        HashMap<String,String> dragArgs = drag.getArgMap();
        assertTrue(dragArgs.containsKey("y"));
        assertEquals(dragArgs.get("y"),"int");
        
        Method start = mmap.get("start"); 
        HashMap<String,String> startArgs = start.getArgMap();
        assertTrue(startArgs.containsKey("x"));
        assertEquals(startArgs.get("x"),"int");
    }
    
}

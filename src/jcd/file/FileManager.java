/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.DataManager;
import jcd.data.DraggablePoint;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import jcd.data.JavaPackage;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author haley.he
 */
public class FileManager implements AppFileComponent{
    static final String TOGGLE_GRID = "grid_toggle";
    static final String TOGGLE_SNAP = "snap_toggle";
    static final String GRID_WIDTH = "grid_width";
    static final String UML_COUNTER = "uml_object_counter";
    
    static final String PROJECT_TREE = "project";
    static final String RELATIONSHIP_LIST = "relationships";
    
    static final String NODE_INDEX = "node_index";
    static final String PARENT_INDEX = "parent_index"; 
    static final String CLASS_TYPE = "class";
    
    //FOR JSON LOADINGM PROPERTIES OF A PACKAGE
    static final String PACKAGE_NAME = "package_name";
    static final String PACKAGE_NUM_CHILDREN = "number_children";
    
    //FOR JSON LOADING, PROPERTIES OF AN UMLOBJECT
    static final String UML_X = "x_position";
    static final String UML_Y = "y_position";
    static final String UML_W = "width";
    static final String UML_NAME_H = "name_height";
    static final String UML_VAR_H = "variable_height";
    static final String UML_METHOD_H = "method_height";
    static final String UML_ABSTRACT = "is_abstract";
    static final String UML_INTERFACE = "is_interface";
    static final String UML_NAME = "name";
    static final String UML_ID = "id";
    static final String UML_PACKAGE_NAME = "package_name";
    static final String UML_SHOW_VARS = "show_variables";
    static final String UML_SHOW_METHODS = "show_methods";
    
    static final String UML_VAR_LIST = "variables";
    static final String UML_METHOD_LIST = "methods";
    static final String UML_PARENT_ID = "parent_id";
    static final String UML_INTERFACE_IDS = "interface_ids";
    
    
    //FOR JSON LOADING, PROPERTIES OF A VARIABLE
    static final String VAR_KEY = "variable_key";
    static final String VAR_NAME = "variable_name";
    static final String VAR_TYPE = "variable_type";
    static final String VAR_ACCESS = "variable_access";
    static final String VAR_STATIC = "variable_is_static";
   
    //FOR JSON LOADING, PROPERTIES OF A METHOD
    static final String METHOD_KEY = "method_key";
    static final String METHOD_NAME = "method_name";
    static final String METHOD_RETURN = "method_type";
    static final String METHOD_ACCESS = "method_access";
    static final String METHOD_STATIC = "method_is_static";
    static final String METHOD_ABSTRACT = "method_is_abstract";
    static final String METHOD_ARG_LIST = "method_arguements";
    static final String METHOD_ARG_NAME = "method_arguement_name";
    static final String METHOD_ARG_TYPE = "method_arguement_type";
    
    static final String RELATIONSHIP_TYPE = "relation_type";
    //FOR JSON LOADING, LINE PROP
    static final String LINE_START_X = "line_start_x";
    static final String LINE_START_Y = "line_start_y";
    static final String LINE_END_X = "line_end_x";
    static final String LINE_END_Y = "line_end_y";
    
        //FOR JSON LOADING, DRAGGABLEPOINT PROP
    static final String PT_X = "point_center_x";
    static final String PT_Y = "point_center_y";

    static final String PT = "point";
    static final String RECT_X = "rectangle_x";
    static final String RECT_Y = "rectangle_y";
    
    static int maxNodeCounter;
    
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        StringWriter sw = new StringWriter();
	DataManager dataManager = (DataManager)data;

	// BUILD THE TREE
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	TreeItem root = dataManager.getProject();
	makeProjectTreeJsonArray(root, arrayBuilder);
	JsonArray nodesArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(TOGGLE_GRID, dataManager.getShowGrid())
                .add(TOGGLE_SNAP, dataManager.getEnforceSnap())
                .add(GRID_WIDTH, dataManager.getGridWidth())
                .add(UML_COUNTER, dataManager.getUMLCounter())
		.add(PROJECT_TREE, nodesArray)
                //.add(RELATIONSHIP_LIST, makeRelationshipJsonArray(dataManager.getRelationships()))
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    private void makeProjectTreeJsonArray (TreeItem root, JsonArrayBuilder arrayBuilder) {
        maxNodeCounter = 0;
        JavaPackage nodeData = (JavaPackage) root.getValue();
        nodeData.setNodeIndex(0);
        nodeData.setParentIndex(-1);
        JsonObject project = makePackageJsonObject(nodeData, root.getChildren().size());
	arrayBuilder.add(project);  
        maxNodeCounter++;
        addChildrenToTreeJsonObject(root, arrayBuilder);
    }
    
    private void addChildrenToTreeJsonObject(TreeItem node, JsonArrayBuilder arrayBuilder) {
        JavaPackage parentData = (JavaPackage) node.getValue();
        ObservableList<TreeItem> children = node.getChildren();
        for (TreeItem child : children) {
            if (child.getValue() instanceof UMLObject) {
                UMLObject childData = (UMLObject) child.getValue();
                childData.setParentIndex(parentData.getNodeIndex());
                childData.setNodeIndex(maxNodeCounter);
                maxNodeCounter++;
                arrayBuilder.add(makeUMLJsonObject(childData));
            } else if (child.getValue() instanceof JavaPackage) {
                JavaPackage childData = (JavaPackage) child.getValue();
                childData.setParentIndex(parentData.getNodeIndex());
                childData.setNodeIndex(maxNodeCounter);
                maxNodeCounter++;
                arrayBuilder.add(makePackageJsonObject(childData, child.getChildren().size()));
                
                addChildrenToTreeJsonObject(child, arrayBuilder);
            }
        }
    }
    
    //HELPER METHOD FOR SAVING A PACKAGE INTO JSON OBJECT
    private JsonObject makePackageJsonObject (JavaPackage jpackage, int numChildren) {
        JsonObject jso = Json.createObjectBuilder()
                .add(CLASS_TYPE, "JavaPackage")
                .add(PACKAGE_NAME, jpackage.getPackageName())
                .add(PACKAGE_NUM_CHILDREN, numChildren)
                .add(NODE_INDEX, jpackage.getNodeIndex())
                .add(PARENT_INDEX, jpackage.getParentIndex())
                .build();
        return jso;
    }
    
    //HELPER METHOD FO SAVING A UMLOBJECT TO A JSON FORMAT
    private JsonObject makeUMLJsonObject (UMLObject umlObj) {
        JsonObject jso = Json.createObjectBuilder()
                .add(CLASS_TYPE, "UMLObject")
                .add(UML_NAME, umlObj.getName())
                .add(UML_ID, umlObj.getID())
                .add(UML_PACKAGE_NAME, umlObj.getPackageName())
                .add(UML_PARENT_ID, umlObj.getParentID())
                .add(UML_INTERFACE, umlObj.getIsInterface())
                .add(UML_ABSTRACT, umlObj.getIsAbstract())
                .add(NODE_INDEX, umlObj.getNodeIndex())
                .add(PARENT_INDEX, umlObj.getParentIndex())
                .add(UML_X, umlObj.getX())
                .add(UML_Y, umlObj.getY())
                .add(UML_W, umlObj.getWidth())
                .add(UML_NAME_H, umlObj.getNameHeight())
                .add(UML_VAR_H, umlObj.getVarHeight())
                .add(UML_METHOD_H, umlObj.getMethodHeight())
                .add(UML_VAR_LIST, makeVarJsonArray(umlObj.getVarMap()))
                .add(UML_SHOW_VARS, umlObj.getShowVars())
                .add(UML_METHOD_LIST, makeMethodJsonArray(umlObj.getMethodMap()))
                .add(UML_SHOW_METHODS, umlObj.getShowMethods())
                .add(UML_INTERFACE_IDS, makeInterfaceJsonArray(umlObj.getInterfaceIDs()))   
                .build();
        return jso;
    }
    
    private JsonArray makeInterfaceJsonArray (ArrayList interfaceIdArray) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (int i=0; i<interfaceIdArray.size(); i++) {
            arrayBuilder.add((int) interfaceIdArray.get(i));
        }
        JsonArray jA = arrayBuilder.build();
	return jA;
    }
    
    //HELPER METHOD TO HELP PROCESS THE VAR HASHMAP OF AN UMLOBJECT
    private JsonArray makeVarJsonArray (HashMap<String,Variable> varMap) {
        Set<String>keys = varMap.keySet();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (String varKeyName: keys) {
            Variable var = varMap.get(varKeyName);
            JsonObject jso = Json.createObjectBuilder()
                .add(VAR_KEY, varKeyName)
		.add(VAR_NAME, var.getName())
		.add(VAR_TYPE, var.getType())
                .add(VAR_ACCESS, var.getAccess().getWord())
		.add(VAR_STATIC, var.getIsStatic())
		.build();
	    arrayBuilder.add(jso);
        }
        JsonArray jA = arrayBuilder.build();
	return jA;
    }
    
    // HELPER METHOD TO HELP PROCESS THE ARGS HASHMAP OF A METHOD
    private JsonArray makeArgsJsonArray(HashMap<String,String> argsMap) {
	Set<String> keys = argsMap.keySet();
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	for (String argName : keys) {
	    String argType = argsMap.get(argName);
	    JsonObject jso = Json.createObjectBuilder()
		.add(METHOD_ARG_NAME, argName)
		.add(METHOD_ARG_TYPE, argType)
		.build();
	    arrayBuilder.add(jso);
	}
	JsonArray jA = arrayBuilder.build();
	return jA;
    }
        
    //HELPER MEHTOD TO HELP PROCESS THE METHOD HASHMAP OF AN UMLOBJECT
    private JsonArray makeMethodJsonArray(HashMap<String, Method> methodMap) {
        Set<String> keys = methodMap.keySet();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (String methodKey : keys) {
            Method method = methodMap.get(methodKey);
            JsonObject jso = Json.createObjectBuilder()
                    .add(METHOD_KEY, methodKey)
                    .add(METHOD_NAME, method.getName())
                    .add(METHOD_RETURN, method.getReturnType())
                    .add(METHOD_ACCESS, method.getAccess().getWord())
                    .add(METHOD_STATIC, method.getIsStatic())
                    .add(METHOD_ABSTRACT, method.getIsAbstract())
                    .add(METHOD_ARG_LIST, makeArgsJsonArray(method.getArgMap()))
                    .build();
            arrayBuilder.add(jso);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;
    }
    
    private JsonArray makeRelationshipJsonArray(ArrayList<ObservableList<Node>> relationships) {
        JsonArrayBuilder relationshipsArrayBuilder = Json.createArrayBuilder();
        for (ObservableList<Node> line : relationships) {
            JsonArrayBuilder lineArrayBuilder = Json.createArrayBuilder();
            //PROCESS ALL OF THE SUBLINES AND POINTS THAT MAKE UP A LINE
            for (int i = 0; i < line.size() - 1; i++) {
                if (line.get(i) instanceof Line) {
                    lineArrayBuilder.add(makeLineJsonObject((Line) line.get(i)));
                } else if (line.get(i) instanceof DraggablePoint) {
                    lineArrayBuilder.add(makePointJsonObject((DraggablePoint) line.get(i)));
                }
            }
            
            //GET LAST NODE, WHICH IS THE RELATIONSHIP SYMBOL OF THE LINE
            Object symbol = line.get(line.size() - 1);
            //CASE1: INHEIRTANCE: TRIANGLE MADE FROM THE POLYGON CLASS 
            if (symbol instanceof Polygon) {
                JsonArrayBuilder trianglePointsBuilder = Json.createArrayBuilder();
                trianglePointsBuilder.add("Inheiritance");
                Polygon polygon = (Polygon) symbol;
                ObservableList<Double> points = polygon.getPoints();
                for (Double point : points) 
                    trianglePointsBuilder.add(point);
                JsonArray triangleArray = trianglePointsBuilder.build();
                lineArrayBuilder.add(triangleArray);
            }            
            //CASE2: ASSOCIATION: ARROW MADE FROM THE POLYLINE CLASS
             else if (symbol instanceof Polyline) {
                JsonArrayBuilder arrowPointsBuilder = Json.createArrayBuilder();
                arrowPointsBuilder.add("Association");
                Polyline polyline = (Polyline) symbol;
                ObservableList<Double> points = polyline.getPoints();
                for (Double point : points) 
                    arrowPointsBuilder.add(point);
                JsonArray arrowArray = arrowPointsBuilder.build();
                lineArrayBuilder.add(arrowArray);
            }
            //AGGREGATION: BOX MADE FRM THE RECTANGLE CLASS
            else if (symbol instanceof Rectangle) {
                Rectangle rect = (Rectangle) symbol;
                JsonObject rectJso = Json.createObjectBuilder()
                        .add(RELATIONSHIP_TYPE, "Aggregation")
                        .add(RECT_X, rect.getX())
                        .add(RECT_Y, rect.getY())
                        .build();
                lineArrayBuilder.add(rectJso);
            }
            JsonArray lineArray = lineArrayBuilder.build();
            
            //ADD THE LINE TO THE RELATIONSHIP JSONARRAY
            relationshipsArrayBuilder.add(lineArray);
        }
        JsonArray jA = relationshipsArrayBuilder.build();
        return jA;
    }
    
    private JsonObject makeLineJsonObject(Line line) {
        JsonObject jso = Json.createObjectBuilder()
                .add(CLASS_TYPE, "Line")
                .add(LINE_START_X, line.getStartX())
                .add(LINE_START_Y, line.getStartY())
                .add(LINE_END_X, line.getEndX())
                .add(LINE_END_Y, line.getEndY())
                .build();
        return jso;
    }
    
    private JsonObject makePointJsonObject(DraggablePoint pt) {
        JsonObject jso = Json.createObjectBuilder()
                .add(CLASS_TYPE, "DraggablePoint")
                .add(PT_X, pt.getCenterX())
                .add(PT_Y, pt.getCenterY())
                .build();
        return jso;
    }
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
        dataManager.setShowGrid(json.getBoolean(TOGGLE_GRID));
        dataManager.setEnforceSnap(json.getBoolean(TOGGLE_SNAP));
        dataManager.setGridWidth(json.getInt(GRID_WIDTH));
        dataManager.setUMLCounter(json.getInt(UML_COUNTER));
        
	// LOAD THE TAG TREE
	JsonArray jsonTreeArray = json.getJsonArray(PROJECT_TREE);
	loadProjectTree(jsonTreeArray, dataManager);
        
        JsonArray jsonRelationsArray = json.getJsonArray(RELATIONSHIP_LIST);
        //loadRelationships(jsonRelationsArray, dataManager);
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    
    private void loadProjectTree (JsonArray projectArray, DataManager dataM) {
        ArrayList<TreeItem> nodes = new ArrayList();
	
	// FIRST UPDATE THE ROOT
	JsonObject rootJso = projectArray.getJsonObject(0);
        JavaPackage rootData = createJavaPackage(rootJso);
        TreeItem src = dataM.getProject();
        //ADD TO LOAD ARRAYLIST
        nodes.add(src);
        //CLEAR AND REPLACE THE PROJECT HEADER
        src.getChildren().clear();
        src.setValue(rootData);
        //CLEAR THE PACKAGE HASHMAP AND THE ROOT TO IT
        HashMap<String,TreeItem> packageMap = dataM.getPackageMap();
        packageMap.clear();
        packageMap.put(rootJso.getString(PACKAGE_NAME).toLowerCase(), src);
        //HAVE CANVAS READY TO ADD UMLOBJS TO
        ArrayList<UMLObject> umlArray = dataM.getUMLObjs();
        HashMap<String,UMLObject> umlMap = dataM.getUMLmap();
        
        // AND NOW JUST GO THROUGH THE REST OF THE ARRAY
	for (int i = 1; i < projectArray.size(); i++) {
	    JsonObject nodeJso = projectArray.getJsonObject(i);
            String nodeType = nodeJso.getString(CLASS_TYPE);
            if (nodeType.equals("JavaPackage")) {
                JavaPackage packageData = createJavaPackage(nodeJso);
                TreeItem newNode = new TreeItem(packageData);
                nodes.add(newNode);
                TreeItem parentNode = nodes.get(packageData.getParentIndex());
                parentNode.getChildren().add(newNode);
                
                packageMap.put(nodeJso.getString(PACKAGE_NAME).toLowerCase(), newNode);
            }
            else if (nodeType.equals("UMLObject")) {
                UMLObject umlData = createUMLObject(nodeJso);
                TreeItem newNode = new TreeItem(umlData);
                nodes.add(newNode);
                TreeItem parentNode = nodes.get(umlData.getParentIndex());
                parentNode.getChildren().add(newNode);
                umlArray.add(umlData);
                umlMap.put(umlData.getName(), umlData);
            }
            
	}
    }
    
    //HELPER METHOD FOR LOADING FROM A JSON FILE. CREATES A JAVAPACKAGE
    private JavaPackage createJavaPackage(JsonObject jpackJso) {
        JavaPackage jpackage = new JavaPackage (jpackJso.getString(PACKAGE_NAME));
        jpackage.setNodeIndex(jpackJso.getInt(NODE_INDEX));
        jpackage.setParentIndex(jpackJso.getInt(PARENT_INDEX));
        return jpackage;
    }
    
    private Variable createVariable (JsonObject varJso) {
        Variable var = new Variable(
                varJso.getString(VAR_NAME),
                varJso.getString(VAR_TYPE),
                varJso.getString(VAR_ACCESS),
                varJso.getBoolean(VAR_STATIC));        
        return var;
    }
    
    private Method createMethod (JsonObject methodJso) {
        Method method = new Method (
                methodJso.getString(METHOD_NAME),
                methodJso.getString(METHOD_RETURN),
                methodJso.getString(METHOD_ACCESS),
                methodJso.getBoolean(METHOD_STATIC),
                methodJso.getBoolean(METHOD_ABSTRACT));  
        
        JsonArray argArray = methodJso.getJsonArray(METHOD_ARG_LIST);
        for (int i=0; i<argArray.size(); i++) {
            JsonObject argJso = argArray.getJsonObject(i);
            method.addArg(argJso.getString(METHOD_ARG_NAME), argJso.getString(METHOD_ARG_TYPE));
        }
        return method;
    }
    
    private UMLObject createUMLObject (JsonObject umlJso) {
        UMLObject uml = new UMLObject(umlJso.getInt(UML_ID), umlJso.getBoolean(UML_INTERFACE));
        uml.setName(umlJso.getString(UML_NAME));
        uml.setPackageName(umlJso.getString(UML_PACKAGE_NAME));
        uml.setIsAbstract(umlJso.getBoolean(UML_ABSTRACT));        
        uml.setNodeIndex(umlJso.getInt(NODE_INDEX));
        uml.setParentIndex(umlJso.getInt(PARENT_INDEX));
        
        //add physical properties @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        uml.setX(umlJso.getInt(UML_X));
        uml.setY(umlJso.getInt(UML_Y));
        uml.setWidth(umlJso.getInt(UML_W));
        uml.setNameHeight(umlJso.getInt(UML_NAME_H));
        uml.setVarHeight(umlJso.getInt(UML_VAR_H));
        uml.setMethodHeight(umlJso.getInt(UML_METHOD_H));
        uml.setHeight(uml.getNameHeight() + uml.getVarHeight() + uml.getMethodHeight());
        
        //add the variables
        JsonArray varJsonArray = umlJso.getJsonArray(UML_VAR_LIST);
        HashMap<String, Variable> varMap= uml.getVarMap();
        for (int i=0; i<varJsonArray.size(); i++) {
            JsonObject varJso = varJsonArray.getJsonObject(i);
            varMap.put(varJso.getString(VAR_KEY),createVariable(varJso));
        }
        uml.setShowVars(umlJso.getBoolean(UML_SHOW_VARS));
        
        //add the methods
        JsonArray methodJsonArray = umlJso.getJsonArray(UML_METHOD_LIST);
        HashMap<String, Method> methodMap = uml.getMethodMap();
        for (int i=0; i<methodJsonArray.size(); i++) {
            JsonObject methodJso = methodJsonArray.getJsonObject(i);
            methodMap.put(methodJso.getString(METHOD_KEY),createMethod(methodJso));
        }
        uml.setShowMethods(umlJso.getBoolean(UML_SHOW_METHODS));
        
        //ADD PARENT CLASS
        uml.setParentID(umlJso.getInt(UML_PARENT_ID));
        
        // ADD INTERFACES TO IMPLEMENT
        JsonArray interfaceJsonArray = umlJso.getJsonArray(UML_INTERFACE_IDS);
        ArrayList<Integer> interfaceIDs = uml.getInterfaceIDs();
        for (int i=0; i < interfaceJsonArray.size(); i++) {
            interfaceIDs.add(interfaceJsonArray.getInt(i));
        }

        return uml;
    }
    
    private void loadRelationships (JsonArray relationsJArray, DataManager dataM) {
        ArrayList<ObservableList<Node>> relationsArray = dataM.getRelationships();
        relationsArray.clear();
        
        for (int i=0; i<relationsJArray.size(); i++) {
            JsonArray lineJArray = relationsJArray.getJsonArray(i);
            ObservableList<Node> lineComps = FXCollections.observableArrayList();
            
            //process all of th points and sub-lines in this relationship line
            for (int j=0; j<lineJArray.size()-1; j++) {
                JsonObject lineCompJso = lineJArray.getJsonObject(j);
                //if it is a line, add a line to the array
                if (lineCompJso.getString(CLASS_TYPE).equals("Line"))
                    lineComps.add(createLineObject(lineCompJso));
                //if  it is a point, add the point to the array
                else if (lineCompJso.getString(CLASS_TYPE).equals("DraggablePoint"))
                    lineComps.add(createDraggablePoint(lineCompJso));
            }
            //final element is the symbol 
            if (lineJArray.get(lineJArray.size()-1) instanceof JsonArray) {
                JsonArray symbol = lineJArray.getJsonArray(lineJArray.size()-1);
                String s = symbol.getString(0);
                //CASE1: ASSOCATION: ARROW (POLYLINE)
                if (s.equals("Association")) 
                    lineComps.add(createPolyline(symbol));
                //CASE2: INHEIRITANCE: TRIANGLE (POLYGON)
                else if (s.equals("Inheiritance")) 
                    lineComps.add(createPolygon(symbol));
            }
            //CASE3: AGGREGATION: RECTANGLE
            else if (lineJArray.get(lineJArray.size()-1) instanceof JsonObject) {
                JsonObject symbol = lineJArray.getJsonObject(lineJArray.size()-1);
                lineComps.add(createRectangle(symbol));
            }
            relationsArray.add(lineComps);
        }
    }
    
    private Line createLineObject(JsonObject lineJso) {
        Line line = new Line(
                getDataAsDouble(lineJso, LINE_START_X),
                getDataAsDouble(lineJso, LINE_START_Y),
                getDataAsDouble(lineJso, LINE_END_X),
                getDataAsDouble(lineJso, LINE_END_Y)
        );
        return line;
    }
    
    private DraggablePoint createDraggablePoint (JsonObject ptJso) {
        DraggablePoint pt = new DraggablePoint (
                getDataAsDouble(ptJso, PT_X),
                getDataAsDouble(ptJso, PT_Y)
        );
        return pt;
    }
    
    private Polyline createPolyline (JsonArray plineJArray) {
        Polyline pline = new Polyline();
        for (int i=1; i<plineJArray.size(); i++) {
            double pt = plineJArray.getJsonNumber(i).doubleValue();
            pline.getPoints().add(pt);
        }
        return pline;
    }
    
    private Polygon createPolygon (JsonArray polygonJArray) {
        Polygon polygon = new Polygon();
        for (int i=1; i<polygonJArray.size(); i++) {
            double pt = polygonJArray.getJsonNumber(i).doubleValue();
            polygon.getPoints().add(pt);
        }
        return polygon;
    }
    
    private Rectangle createRectangle (JsonObject rectJso) {
        Rectangle rect = new Rectangle(getDataAsDouble(rectJso, RECT_X), getDataAsDouble(rectJso, RECT_Y),5,5);
        rect.setFill(Color.WHITE);
        rect.setStroke(Color.BLACK);
        rect.rotateProperty().set(45);
        return rect;
    }
    
    private double getDataAsDouble (JsonObject jso, String dataName) {
        JsonValue value = jso.get(dataName);
        JsonNumber num = (JsonNumber) value;
        return num.doubleValue();
    }
  
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }

    public void saveTestData(TreeItem root, ArrayList<ObservableList<Node>> relation, String filePath) throws IOException {
        StringWriter sw = new StringWriter();

	// BUILD THE TREE
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	makeProjectTreeJsonArray(root, arrayBuilder);
	JsonArray nodesArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(PROJECT_TREE, nodesArray)
                .add(RELATIONSHIP_LIST, makeRelationshipJsonArray(relation))
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
}

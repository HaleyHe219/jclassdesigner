/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import jcd.data.DataManager;
import jcd.data.DraggablePoint;
import jcd.data.JavaPackage;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import jcd.file.FileManager;

/**
 *
 * @author haley.he
 */
public class TestSave {
    static final String PATH = "./work";
    static final String FILE_NAME = "DesignSaveTest.json";
    
    public static void main(String[] args) {
        File file = new File(PATH, FILE_NAME);
        
        TreeItem root = new TreeItem(new JavaPackage("src"));
                TreeItem def = new TreeItem(new JavaPackage("DefaultPackage"));
        TreeItem java = new TreeItem(new JavaPackage("java"));
        TreeItem util = new TreeItem(new JavaPackage("java.util"));
        java.getChildren().add(util);
        TreeItem javafx = new TreeItem(new JavaPackage("javafx"));
        root.getChildren().addAll(def, java, javafx);

        UMLObject application = new UMLObject(1,false);
        setSettings(application, "Application", "javafx.application",500, 100);
        application.setIsAbstract(true);
        addMethod(application, "start", "void", "default", false, true, "primaryStage", "Stage");
        TreeItem applicationItem = new TreeItem(application);
        javafx.getChildren().add(applicationItem);

        UMLObject threadExample = new UMLObject(2,false);
        threadExample.setParentClass(application);
        setSettings(threadExample, "ThreadExample", "DefaultPackage", 400, 400);
        addVariable(threadExample, "START_TEXT", "String", "public", true);
        addVariable(threadExample, "PAUSE_TEXT", "String", "public", true);
        addVariable(threadExample, "window", "Stage", "private", false);
        addVariable(threadExample, "appPane", "BorderPane", "private", false);
        addVariable(threadExample, "topPane", "FlowPane", "private", false);
        addVariable(threadExample, "startButton", "Button", "private", false);
        addVariable(threadExample, "pauseButton", "Button", "private", false);
        addVariable(threadExample, "scrollPane", "ScrollPane", "private", false);
        addVariable(threadExample, "textArea", "TextArea", "private", false);
        addVariable(threadExample, "dateThread", "Thread", "private", false);
        addVariable(threadExample, "dateTask", "Task", "private", false);
        addVariable(threadExample, "counterThread", "Thread", "private", false);
        addVariable(threadExample, "counterTask", "Task", "private", false);
        addVariable(threadExample, "work", "boolean", "private", false);
        addMethod(threadExample, "start", "void", "public", false, false, "primaryStage", "Stage");
        addMethod(threadExample, "startWork", "void", "public", false, false, "", "");
        addMethod(threadExample, "pauseWork", "void", "public", false, false, "", "");
        addMethod(threadExample, "doWork", "boolean", "public", false, false, "", "");
        addMethod(threadExample, "appendText", "void", "public", false, false, "textToAppend", "String");
        addMethod(threadExample, "sleep", "void", "public", false, false, "timeToSleep", "int");
        addMethod(threadExample, "initLayout", "void", "private", false, false, "", "");
        addMethod(threadExample, "initWindow", "void", "private", false, false, "initPrimaryStage", "Stage");
        addMethod(threadExample, "initThreads", "void", "private", false, false, "", "");
        addMethod(threadExample, "main", "void", "public", true, false, "args", "String[]");
        TreeItem threadExItem = new TreeItem(threadExample);
        def.getChildren().add(threadExItem);

        UMLObject eventHandler = new UMLObject(3,true);
        setSettings(eventHandler, "EventHandler", "javafx.event", 300, 0);
        addMethod(eventHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem eh = new TreeItem(eventHandler);
        javafx.getChildren().add(eh);
        
        UMLObject pauseHandler = new UMLObject(4,false);
        pauseHandler.getInterfaceIDs().add(3);
        setSettings(pauseHandler, "PauseHandler", "DefaultPackage", 600, 500);
        addVariable(pauseHandler, "app", "ThreadExample", "private", false);
        addMethod(pauseHandler, "PauseHandler", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(pauseHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem ph = new TreeItem(pauseHandler);
        def.getChildren().add(ph);

        UMLObject startHandler = new UMLObject(5,false);
        startHandler.getInterfaceIDs().add(3);
        setSettings(startHandler, "StartHandler", "DefaultPackage", 600, 700);
        addVariable(startHandler, "app", "ThreadExample", "private", false);
        addMethod(startHandler, "StartHandler", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(startHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem sh = new TreeItem(startHandler);
        def.getChildren().add(sh);

        UMLObject task = createAPI2(6,"Task", "javafx.concurrent",900,0, false,javafx);
        
        UMLObject counterTask = new UMLObject(7,false);
        counterTask.setParentClass(task);
        setSettings(counterTask, "CounterTask", "DefaultPackage", 200, 200);
        addVariable(counterTask, "app", "ThreadExample", "private", false);
        addVariable(counterTask, "counter", "int", "private", false);
        addMethod(counterTask, "CounterTask", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(counterTask, "call", "Void", "protected", false, false, "", "");
        TreeItem ct = new TreeItem(counterTask);
        def.getChildren().add(ct);

        UMLObject dateTask = new UMLObject(8,false);
        dateTask.setParentClass(task);
        setSettings(dateTask, "DateTask", "DefaultPackage", 200, 700);
        addVariable(dateTask, "app", "ThreadExample", "private", false);
        addVariable(dateTask, "now", "Date", "private", false);
        addMethod(dateTask, "DateTask", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(dateTask, "call", "Void", "protected", false, false, "", "");
        TreeItem dt = new TreeItem(dateTask);
        def.getChildren().add(dt);
        
        
        createAPI(9,"Platform", "javafx.application", 900,125, false, javafx);
        createAPI(10,"Date","java.util", 900,250, false, java);
        createAPI(11,"Stage","javafx.stage", 900,375, false,javafx);
        createAPI(12,"BorderPane","javafx.scene.layout",900,500, false,javafx);
        createAPI(13,"FlowPane","javafx.scene.layout",900,625, false,javafx);
        createAPI(14,"Button","javafx.scene.control",900,750, false,javafx);
        createAPI(15,"ScrollPane","javafx.scene.control",700,0, false,javafx);
        createAPI(16,"TextArea","javafx.scene.control", 700,125, false,javafx);
        createAPI(17,"Thread","java.lang",700,250, false,java);
        createAPI(18,"Event","javafx.event",700,375,false,javafx);
        
        /*
        ArrayList<ObservableList<Node>> relations = new ArrayList();
        relations.add(addInheritance (10,100,100,100));
        relations.add(addAggregation (10,200,100,200));
        relations.add(addAssociation (1000,300,200,300));
        */
        
        DataManager dataM; dataM = new DataManager();
        dataM.reset();
        dataM.setProject(root);
        //dataM.setRelationships(relations);
        dataM.setUMLCounter(19);
        
        FileManager fm = new FileManager();
        try {
            fm.saveData(dataM, file.getPath());
        } catch (IOException ex) {
            System.out.println("FAILED");
        }
    }

    public static void addVariable(UMLObject uml, String name, String type, String access, Boolean isStatic) {
        Variable v = new Variable(name, type, access, isStatic);
        uml.getVarMap().put(v.getName(), v);
    }

    public static void addMethod(UMLObject uml, String name, String returnType, String access,
            boolean isStatic, boolean isAbstract, String argName, String argType) {
        Method m = new Method(name, returnType, access, isStatic, isAbstract);
        if (argName.length() > 0 && argType.length() > 0) {
            m.addArg(argName, argType);
        }
        uml.getMethodMap().put(m.getName(), m);
    }

    public static void setSettings(UMLObject uml, String name, String pname, int x, int y) {
        uml.setName(name);
        uml.setPackageName(pname);
        uml.setNameHeight(100);
        //uml.setVarHeight(100);
        //uml.setMethodHeight(100);
        uml.setLocationAndSize(x, y, uml.getWidth(), 0); //last parameter does nothing
    }
    
    public static void createAPI (int id, String name, String packageName, int x, int y, boolean isI, TreeItem parent) {
        UMLObject u = new UMLObject(id, isI);
        u.setName(name);
        u.setPackageName(packageName);
        u.setLocationAndSize(x, y, u.getWidth(), 0); //last parameter does nothing
        TreeItem uItem = new TreeItem(u);
        parent.getChildren().add(uItem);
    }
    
    public static UMLObject createAPI2 (int id, String name, String packageName, int x, int y, boolean isI, TreeItem parent) {
        UMLObject u = new UMLObject(id, isI);
        u.setName(name);
        u.setPackageName(packageName);
        u.setLocationAndSize(x, y, u.getWidth(), 0); //last parameter does nothing
        TreeItem uItem = new TreeItem(u);
        parent.getChildren().add(uItem);
        return u;
    }
    
    public static ObservableList<Node> addAggregation (double startX, double startY, double endX, double endY) {
        ObservableList<Node> relation = FXCollections.observableArrayList();
        
        Line line = new Line (startX,startY, (startX+endX)/2, (startY+endY)/2);
                relation.add(line);
        DraggablePoint pt = new DraggablePoint((startX+endX)/2, (startY+endY)/2);
                relation.add(pt);
        Line line2 = new Line ((startX+endX)/2, (startY+endY)/2, endX, endY);
                relation.add(line2);   
        Rectangle r = new Rectangle (startX,startY-5,10,10);
            r.setFill(Color.WHITE);
            r.setStroke(Color.BLACK);
            r.rotateProperty().set(45);
                relation.add(r);
        return relation;
    }
    
    public static ObservableList<Node> addInheritance (double startX, double startY, double endX, double endY) {
        ObservableList<Node> relation = FXCollections.observableArrayList();
        
        Line line = new Line (startX,startY, (startX+endX)/2, (startY+endY)/2);
                relation.add(line);
        DraggablePoint pt = new DraggablePoint((startX+endX)/2, (startY+endY)/2);
                relation.add(pt);
        Line line2 = new Line ((startX+endX)/2, (startY+endY)/2, endX, endY);
                relation.add(line2);    
        Polygon polygon = new Polygon(
            endX, endY,
            endX-10.0, endY+5.0,
            endX-10.0, endY-5.0 );
        polygon.setFill(Color.WHITE);
        polygon.setStroke(Color.BLACK);
        relation.add(polygon);
        
        return relation;
    }
    
    public static ObservableList<Node> addAssociation (double startX, double startY, double endX, double endY) {
        ObservableList<Node> relation = FXCollections.observableArrayList();
        
        Line line = new Line (startX,startY, (startX+endX)/2, (startY+endY)/2);
                relation.add(line);
        DraggablePoint pt = new DraggablePoint((startX+endX)/2, (startY+endY)/2);
                relation.add(pt);
        Line line2 = new Line ((startX+endX)/2, (startY+endY)/2, endX, endY);
                relation.add(line2);  
        Polyline polyline = new Polyline(
            endX-10.0, endY+5.0,
            endX, endY,
            endX-10.0, endY-5.0 );
        polyline.setFill(Color.WHITE);
        polyline.setStroke(Color.BLACK);
        relation.add(polyline);
        
        return relation;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import jcd.data.DataManager;
import jcd.data.JavaPackage;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import jcd.file.FileManager;
import saf.AppTemplate;

/**
 *
 * @author haley.he
 */
public class TestLoad {
    DataManager dataM;
    static final String FILE_PATH = "./work/DesignSaveTest.json";
    static final String FILE_PATH2 = "./work/DesignSaveTest2.json";
    static final String FILE_PATH3 = "./work/DesignSaveTest3.json";
    static final String FILE_PATH4 = "./work/m";
    
    
    public static void main (String [] args) {
        DataManager dataM = new DataManager();
        FileManager fm = new FileManager();
        try {
            fm.loadData(dataM, FILE_PATH);
        } catch (IOException ex) {
            System.out.println("FAIL");
        }
        
        TreeItem root = dataM.getProject();
        //printTreeInfo(root);
        System.out.println("----------------------------");
        System.out.println();
        
        ArrayList<UMLObject> umlArray = dataM.getUMLObjs();
        printClassInfo(umlArray);
        System.out.println();
        
        ArrayList<ObservableList<Node>> relations = dataM.getRelationships();
        //printRelationInfo(relations);
        
        //System.out.println(umlArray.get(1).exportToCode(dataM));
        
        HashMap<String,TreeItem> packages = dataM.getPackageMap();
        //printPackageMap(packages);
    }

    
    public static void printRelationInfo (ArrayList<ArrayList<Node>> relations) {
        for (ArrayList<Node> line: relations) {
            for (Node node: line)
                System.out.println(node.getClass());
            System.out.println("--");
        }
    }
    
    public static void printClassInfo (ArrayList<UMLObject> umlArray) {
         for (UMLObject uml: umlArray) {
            if (uml.getIsInterface())
                System.out.println(uml.getID() + ") " + "Interface Name: " + uml.getName());
            else 
                System.out.println(uml.getID() + ") " + "Class Name: " + uml.getName());
            System.out.println("Package Name: " + uml.getPackageName());
            
            /*
            HashMap<String,Variable> varMap = uml.getVarMap();
            Set<String> keySet = varMap.keySet();
            for (String key: keySet) {
                Variable v = varMap.get(key);
                System.out.println(v.toJavaCode());
            }
            
            HashMap<String,Method> methodMap = uml.getMethodMap();
            keySet = methodMap.keySet();
            for (String key: keySet) {
                Method m = methodMap.get(key);
                System.out.println(m.toJavaCode());
            }
*/
            System.out.println("------------------------------------------");
        }
    }
    
    public static void printTreeInfo (TreeItem root) {
        JavaPackage rootData = (JavaPackage) root.getValue();
        System.out.println(rootData.getNodeIndex() + ") " + rootData.getPackageName());
        
        for (int i=0; i<root.getChildren().size(); i++) {
            Object node = ((TreeItem)root.getChildren().get(i)).getValue();
            if (node instanceof UMLObject) {
                UMLObject uml = (UMLObject) node;
                System.out.println(uml.getNodeIndex() + ") " + uml.getName() + " - " + uml.getPackageName());
            }
            else if (node instanceof JavaPackage) {
                printTreeInfo((TreeItem) root.getChildren().get(i));
            }
            else 
                System.out.println("FAIL");
        }
     }
    
    public static void printPackageMap (HashMap<String,TreeItem> packages) {
        Set<String>keys = packages.keySet();
        for (String k : keys) {
            JavaPackage j =(JavaPackage) packages.get(k).getValue();
            System.out.println(k + " - " + j.getPackageName());
        }
    } 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import jcd.data.JavaPackage;
import jcd.data.UMLObject;
import jcd.file.FileManager;
import static jcd.test_bed.TestSave.addAggregation;
import static jcd.test_bed.TestSave.addAssociation;
import static jcd.test_bed.TestSave.addInheritance;
import static jcd.test_bed.TestSave.addMethod;
import static jcd.test_bed.TestSave.addVariable;
import static jcd.test_bed.TestSave.createAPI;
import static jcd.test_bed.TestSave.createAPI2;
import static jcd.test_bed.TestSave.setSettings;

/**
 *
 * @author haley.he
 */
public class TestSave3 {
 static final String PATH = "./work";
    static final String FILE_NAME = "DesignSaveTest3.json";
    
    public static void main(String[] args) {
        File file = new File(PATH, FILE_NAME);
        
        TreeItem root = new TreeItem(new JavaPackage("src"));
        TreeItem java = new TreeItem(new JavaPackage("java"));
        TreeItem util = new TreeItem(new JavaPackage("java.util"));
        java.getChildren().add(util);
        TreeItem def = new TreeItem(new JavaPackage("DefaultPackage"));
        TreeItem a = new TreeItem(new JavaPackage("DefaultPackage.a"));
        def.getChildren().add(a);
        TreeItem javafx = new TreeItem(new JavaPackage("javafx"));
        root.getChildren().addAll(def, java, javafx);

        UMLObject draggable = new UMLObject(true);
        setSettings(draggable, "DragInterface", "DefaultPackage", 10, 10);
        addMethod(draggable, "start", "void", "public", false, true, "x", "int");
        addMethod(draggable, "drag", "void", "public", false, true, "y", "int");
        TreeItem d = new TreeItem(draggable);
        def.getChildren().add(d);
        
        
        UMLObject application = new UMLObject(false);
        setSettings(application, "Application", "javafx.application",500, 100);
        application.setIsAbstract(true);
        addMethod(application, "start", "void", "default", false, true, "primaryStage", "Stage");
        TreeItem applicationItem = new TreeItem(application);
        javafx.getChildren().add(applicationItem);

        UMLObject threadExample = new UMLObject(false);
                threadExample.setParent(application);
        setSettings(threadExample, "ThreadExample", "DefaultPackage", 400, 400);
        addVariable(threadExample, "START_TEXT", "String", "public", true);
        addVariable(threadExample, "PAUSE_TEXT", "String", "public", true);
        addVariable(threadExample, "window", "Stage", "private", false);
        addVariable(threadExample, "appPane", "BorderPane", "private", false);
        addVariable(threadExample, "topPane", "FlowPane", "private", false);
        addVariable(threadExample, "startButton", "Button", "private", false);
        addVariable(threadExample, "pauseButton", "Button", "private", false);
        addVariable(threadExample, "scrollPane", "ScrollPane", "private", false);
        addVariable(threadExample, "textArea", "TextArea", "private", false);
        addVariable(threadExample, "dateThread", "Thread", "private", false);
        addVariable(threadExample, "dateTask", "Task", "private", false);
        addVariable(threadExample, "counterThread", "Thread", "private", false);
        addVariable(threadExample, "counterTask", "Task", "private", false);
        addVariable(threadExample, "work", "boolean", "private", false);
        addMethod(threadExample, "start", "void", "public", false, false, "primaryStage", "Stage");
        addMethod(threadExample, "startWork", "void", "public", false, false, "", "");
        addMethod(threadExample, "pauseWork", "void", "public", false, false, "", "");
        addMethod(threadExample, "doWork", "boolean", "public", false, false, "", "");
        addMethod(threadExample, "appendText", "void", "public", false, false, "textToAppend", "String");
        addMethod(threadExample, "sleep", "void", "public", false, false, "timeToSleep", "int");
        addMethod(threadExample, "initLayout", "void", "private", false, false, "", "");
        addMethod(threadExample, "initWindow", "void", "private", false, false, "initPrimaryStage", "Stage");
        addMethod(threadExample, "initThreads", "void", "private", false, false, "", "");
        addMethod(threadExample, "main", "void", "public", true, false, "args", "String[]");
        TreeItem threadExItem = new TreeItem(threadExample);
        def.getChildren().add(threadExItem);

        UMLObject eventHandler = new UMLObject(true);
        setSettings(eventHandler, "EventHandler", "javafx.event", 300, 0);
        addMethod(eventHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem eh = new TreeItem(eventHandler);
        javafx.getChildren().add(eh);
        
        UMLObject pauseHandler = new UMLObject(false);
        pauseHandler.setParent(eventHandler);
        setSettings(pauseHandler, "PauseHandler", "DefaultPackage", 600, 500);
        addVariable(pauseHandler, "app", "ThreadExample", "private", false);
        addMethod(pauseHandler, "PauseHandler", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(pauseHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem ph = new TreeItem(pauseHandler);
        def.getChildren().add(ph);

        UMLObject startHandler = new UMLObject(false);
        startHandler.setParent(eventHandler);
        setSettings(startHandler, "StartHandler", "DefaultPackage", 600, 700);
        addVariable(startHandler, "app", "ThreadExample", "private", false);
        addMethod(startHandler, "StartHandler", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(startHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem sh = new TreeItem(startHandler);
        def.getChildren().add(sh);
        
                UMLObject task = createAPI2("Task", "javafx.concurrent",900,0, false,javafx);

        UMLObject counterTask = new UMLObject(false);
        counterTask.setParent(task);
        setSettings(counterTask, "CounterTask", "DefaultPackage", 200, 200);
        addVariable(counterTask, "app", "ThreadExample", "private", false);
        addVariable(counterTask, "counter", "int", "private", false);
        addMethod(counterTask, "CounterTask", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(counterTask, "call", "Void", "protected", false, false, "", "");
        TreeItem ct = new TreeItem(counterTask);
        def.getChildren().add(ct);

        UMLObject dateTask = new UMLObject(false);
        dateTask.setParent(task);
        setSettings(dateTask, "DateTask", "DefaultPackage", 200, 700);
        addVariable(dateTask, "app", "ThreadExample", "private", false);
        addVariable(dateTask, "now", "Date", "private", false);
        addMethod(dateTask, "DateTask", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(dateTask, "call", "Void", "protected", false, false, "", "");
        TreeItem dt = new TreeItem(dateTask);
        def.getChildren().add(dt);
        
        createAPI("Platform", "javafx.application", 900,125, false, javafx);
        createAPI("Date","java.util", 900,250, false, java);
        createAPI("Stage","javafx.stage", 900,375, false,javafx);
        createAPI("BorderPane","javafx.scene.layout",900,500, false,javafx);
        createAPI("FlowPane","javafx.scene.layout",900,625, false,javafx);
        createAPI("Button","javafx.scene.control",900,750, false,javafx);
        createAPI("ScrollPane","javafx.scene.control",700,0, false,javafx);
        createAPI("TextArea","javafx.scene.control", 700,125, false,javafx);
        createAPI("Thread","java.lang",700,250, false,java);
                createAPI("Event","javafx.event",700,375,false,javafx);
        
        ArrayList<ArrayList<Node>> relations = new ArrayList<>();
        relations.add(addAggregation (400,300,100,100));
        relations.add(addAssociation (10,200,100,200));
        relations.add(addAggregation (100,300,200,300));
        
        UMLObject dummy = new UMLObject(false);
        setSettings(dummy, "Dummy", "DefaultPackage.a", 0, 400);
        addVariable(dummy, "hello", "int", "protected", false);
                addMethod(dummy, "yell", "void", "public", false, false, "", "");
                TreeItem dum = new TreeItem(dummy);
                a.getChildren().add(dum);

        FileManager fm = new FileManager();
        try {
            fm.saveTestData(root, relations, file.getPath());
        } catch (IOException ex) {
            System.out.println("FAILED");
        }
    }    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.test_bed;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import jcd.data.JavaPackage;
import jcd.data.UMLObject;
import jcd.file.FileManager;
import static jcd.test_bed.TestSave.addMethod;
import static jcd.test_bed.TestSave.addVariable;
import static jcd.test_bed.TestSave.createAPI;
import static jcd.test_bed.TestSave.createAPI2;
import static jcd.test_bed.TestSave.setSettings;

/**
 *
 * @author haley.he
 */
public class TestSave2 {
    static final String PATH = "./work";
    static final String FILE_NAME2 = "DesignSaveTest2.json";
    
    public static void main(String[] args) {
        File file = new File(PATH, FILE_NAME2);
        
        TreeItem root = new TreeItem(new JavaPackage("src"));
        TreeItem java = new TreeItem(new JavaPackage("java"));
        TreeItem util = new TreeItem(new JavaPackage("java.util"));
        java.getChildren().add(util);
        TreeItem def = new TreeItem(new JavaPackage("DefaultPackage"));
        TreeItem a = new TreeItem(new JavaPackage("DefaultPackage.a"));
        def.getChildren().add(a);
        TreeItem javafx = new TreeItem(new JavaPackage("javafx"));
        root.getChildren().addAll(def, java, javafx);

        UMLObject aClass = new UMLObject(false);
        setSettings(aClass, "AbstractClass", "DefaultPackage",10, 10);
        aClass.setIsAbstract(true);
        TestSave.addMethod(aClass, "start", "void", "public", false, true, "primaryStage", "Stage");
        TestSave.addMethod(aClass, "doHw", "void", "public", false, false, "cry", "int");
        TreeItem aClassItem = new TreeItem(aClass);
        def.getChildren().add(aClassItem);

        UMLObject application = new UMLObject(false);
        setSettings(application, "Application", "javafx.application",500, 100);
        application.setIsAbstract(true);
        addMethod(application, "start", "void", "default", false, true, "primaryStage", "Stage");
        TreeItem applicationItem = new TreeItem(application);
        javafx.getChildren().add(applicationItem);

        UMLObject threadExample = new UMLObject(false);
        threadExample.setParent(application);
        setSettings(threadExample, "ThreadExample", "DefaultPackage", 400, 400);
        addVariable(threadExample, "START_TEXT", "String", "public", true);
        addVariable(threadExample, "PAUSE_TEXT", "String", "public", true);
        addVariable(threadExample, "window", "Stage", "private", false);
        addVariable(threadExample, "appPane", "BorderPane", "private", false);
        addVariable(threadExample, "topPane", "FlowPane", "private", false);
        addVariable(threadExample, "startButton", "Button", "private", false);
        addVariable(threadExample, "pauseButton", "Button", "private", false);
        addVariable(threadExample, "scrollPane", "ScrollPane", "private", false);
        addVariable(threadExample, "textArea", "TextArea", "private", false);
        addVariable(threadExample, "dateThread", "Thread", "private", false);
        addVariable(threadExample, "dateTask", "Task", "private", false);
        addVariable(threadExample, "counterThread", "Thread", "private", false);
        addVariable(threadExample, "counterTask", "Task", "private", false);
        addVariable(threadExample, "work", "boolean", "private", false);
        addMethod(threadExample, "start", "void", "public", false, false, "primaryStage", "Stage");
        addMethod(threadExample, "startWork", "void", "public", false, false, "", "");
        addMethod(threadExample, "pauseWork", "void", "public", false, false, "", "");
        addMethod(threadExample, "doWork", "boolean", "public", false, false, "", "");
        addMethod(threadExample, "appendText", "void", "public", false, false, "textToAppend", "String");
        addMethod(threadExample, "sleep", "void", "public", false, false, "timeToSleep", "int");
        addMethod(threadExample, "initLayout", "void", "private", false, false, "", "");
        addMethod(threadExample, "initWindow", "void", "private", false, false, "initPrimaryStage", "Stage");
        addMethod(threadExample, "initThreads", "void", "private", false, false, "", "");
        addMethod(threadExample, "main", "void", "public", true, false, "args", "String[]");
        TreeItem threadExItem = new TreeItem(threadExample);
        def.getChildren().add(threadExItem);

        UMLObject eventHandler = new UMLObject(true);
        setSettings(eventHandler, "EventHandler", "javafx.event", 300, 800);
        addMethod(eventHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem eh = new TreeItem(eventHandler);
        javafx.getChildren().add(eh);
        
        UMLObject pauseHandler = new UMLObject(false);
        pauseHandler.setParent(eventHandler);
        setSettings(pauseHandler, "PauseHandler", "DefaultPackage", 600, 500);
        addVariable(pauseHandler, "app", "ThreadExample", "private", false);
                addVariable(pauseHandler, "smile","int","public",false);
        addMethod(pauseHandler, "PauseHandler", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(pauseHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem ph = new TreeItem(pauseHandler);
        def.getChildren().add(ph);

        UMLObject startHandler = new UMLObject(false);
                startHandler.setParent(eventHandler);
        setSettings(startHandler, "StartHandler", "DefaultPackage", 600, 700);
        addVariable(startHandler, "app", "ThreadExample", "private", false);
        addMethod(startHandler, "StartHandler", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(startHandler, "handle", "void", "public", false, false, "event", "Event");
        TreeItem sh = new TreeItem(startHandler);
        def.getChildren().add(sh);

        UMLObject task = createAPI2("Task", "javafx.concurrent",900,0, false,javafx);
                
        UMLObject counterTask = new UMLObject(false);
        counterTask.setParent(task);
        setSettings(counterTask, "CounterTask", "DefaultPackage", 200, 300);
        addVariable(counterTask, "app", "ThreadExample", "private", false);
        addVariable(counterTask, "counter", "int", "private", false);
        addMethod(counterTask, "CounterTask", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(counterTask, "call", "Void", "protected", false, false, "", "");
        TreeItem ct = new TreeItem(counterTask);
        def.getChildren().add(ct);

        UMLObject dateTask = new UMLObject(false);
        dateTask.setParent(task);
        setSettings(dateTask, "DateTask", "DefaultPackage", 200, 500);
        addVariable(dateTask, "app", "ThreadExample", "private", false);
        addVariable(dateTask, "now", "Date", "private", false);
        addMethod(dateTask, "DateTask", "", "public", false, false, "initApp", "ThreadExample");
        addMethod(dateTask, "call", "Void", "protected", false, false, "", "");
        TreeItem dt = new TreeItem(dateTask);
        def.getChildren().add(dt);
         
        createAPI("Platform", "javafx.application", 900,125, false, javafx);
        createAPI("Date","java.util", 900,250, false, java);
        createAPI("Stage","javafx.stage", 900,375, false,javafx);
        createAPI("BorderPane","javafx.scene.layout",900,500, false,javafx);
        createAPI("FlowPane","javafx.scene.layout",900,625, false,javafx);
        createAPI("Button","javafx.scene.control",900,750, false,javafx);
        createAPI("ScrollPane","javafx.scene.control",700,0, false,javafx);
        createAPI("TextArea","javafx.scene.control", 700,125, false,javafx);
        createAPI("Thread","java.lang",700,250, false,java);
        createAPI("Event","javafx.event",700,375,false,javafx);
        
        UMLObject random = new UMLObject(false);
        setSettings(random, "Random", "DefaultPackage", 0, 500);
        addMethod(pauseHandler, "cry", "void", "public", false, false, "tear", "int");
        TreeItem r = new TreeItem(random);
        def.getChildren().add(r);
        
        ArrayList<ArrayList<Node>> relations = new ArrayList<>();
        relations.add(TestSave.addAssociation (10,200,200,200));
        relations.add(TestSave.addAggregation (500,50,600,50));
        relations.add(TestSave.addAssociation (123,234,345,456));
        relations.add(TestSave.addAssociation (1000,300,200,300));
        

        FileManager fm = new FileManager();
        try {
            fm.saveTestData(root, relations, file.getPath());
        } catch (IOException ex) {
            System.out.println("FAILED");
        }
    }
}

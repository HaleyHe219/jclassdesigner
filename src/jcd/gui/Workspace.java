/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.util.Callback;
import javafx.util.StringConverter;
import static jcd.PropertyType.*;
import jcd.control.CanvasController;
import jcd.control.ComponentController;
import jcd.control.ToolbarController;
import jcd.data.DataManager;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import saf.ui.AppGUI;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;

/**
 *
 * @author haley.he
 */
public class Workspace extends AppWorkspaceComponent {
    AppTemplate app;
    AppGUI gui;
    
    public static final String COMPONENT_CSS = "component";
    public static final String CANVAS_CSS = "canvas";
    public static final double SCROLL_PANE_WIDTH = 300;
    public static final double SCROLL_PANE_HEIGHT = 200;
    
    //Rendering surface of the UML classes/interfaces
    Pane canvas;
    
    //Display for altering the information of a class/interface
    VBox componentToolbar;
        GridPane names;
            TextField className;
            TextField packageName;
            ChoiceBox<UMLObject> parentClass;
            ScrollPane interfaceScrollPane;
            ListView<UMLObject> interfaceList;

        ScrollPane varScrollPane; 
        HBox varHeader;
            Button addVarButton;
            Button removeVarButton;
        TableView<Variable>varTable;
            TableColumn<Variable,String> varNameCol;
            TableColumn<Variable,String> varTypeCol;
            TableColumn<Variable,Boolean> varStaticCol;
            TableColumn<Variable,String> varAccessCol;

        ScrollPane methodScrollPane;
        HBox methodHeader;
            Button addMethodButton;
            Button removeMethodButton;
            Button addArgButton;
            Button removeArgButton;
        TableView<Method> methodTable;
            TableColumn<Method,String> methodNameCol;
            TableColumn<Method,String> methodReturnCol;
            TableColumn<Method,Boolean> methodStaticCol;
            TableColumn<Method,Boolean> methodAbstractCol;
            TableColumn<Method,String> methodAccessCol;
            TableColumn methodArgsCol;
            ObservableList<TableColumn> argNameCols;
            ObservableList<TableColumn> argTypeCols;
    
    HBox showPane;
    CheckBox showVars;
    CheckBox showMethods;
    
    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    ArrayList<Line> lines;
    ObservableList<Variable> varData;
    ObservableList<Method> methodData;
    
    Boolean activateControllers = true;
    
    public Workspace(AppTemplate initApp) {
        app = initApp;
        this.gui = app.getGUI();
        
        layoutWorkspace();
        setupToolbarHandlers();
        setupCanvasHandlers();
        setupComponentHandlers();
        
        varData = FXCollections.observableArrayList();
        methodData = FXCollections.observableArrayList();
        
        DataManager dataM = (DataManager)app.getDataComponent();
    }
    
    @Override
    public void reloadWorkspace() {
        activateControllers = false;
        DataManager dataM = (DataManager) app.getDataComponent();
        canvas.getChildren().clear();        
        createGridLines();
        ArrayList<UMLObject> umls = dataM.getUMLObjs();
        for (UMLObject uml: umls) {
            uml.start((int) uml.getX(), (int) uml.getY());
            uml.updateUMLContent();
            canvas.getChildren().add(uml);
        }
        
        for (UMLObject uml: umls) {       
            dataM.generateParentLines(uml);
            dataM.generateInterfaceLines(uml);
            dataM.generateMethAssociationLines(uml);
            dataM.generateMethAssociationLines(uml);
            dataM.generateAggregateLines(uml);
        }
        loadNoSelectedUMLSetting();
        activateControllers = true;
    }
    
    public void reloadLines() {
        activateControllers = false;
        DataManager dataM = (DataManager) app.getDataComponent();
        for (ObservableList<Node> relation: dataM.getRelationships()) {
            canvas.getChildren().removeAll(relation);
        }
        ArrayList<UMLObject> umls = dataM.getUMLObjs();
        for (int i=0; i<umls.size(); i++) {       
            UMLObject uml = umls.get(i);
            dataM.generateParentLines(uml);
            dataM.generateInterfaceLines(uml);
            dataM.addAPIs(uml, "method");
            dataM.generateMethAssociationLines(uml);
            dataM.addAPIs(uml, "arg");
            dataM.generateArgAssociationLines(uml);
            dataM.addAPIs(uml, "var");
            dataM.generateAggregateLines(uml);
        }
        if (dataM.getSelectedUML() != null) 
            loadSelectedUMLSettings(dataM.getSelectedUML());
        else
            loadNoSelectedUMLSetting();
        activateControllers = true;
    }

    @Override
    public void initStyle() {
        componentToolbar.getStyleClass().add(COMPONENT_CSS);
        canvas.getStyleClass().add(CANVAS_CSS);
    }
    
    private void layoutWorkspace() {
        canvas = new Pane();
        createGridLines();

        componentToolbar = new VBox();
        names = new GridPane();
        names.add(new Label("Name:"), 0, 0);
        names.add(new Label("Package:"), 0, 1);
        names.add(new Label("Parent Class:"), 0, 2);
        names.add(new Label("Parent Interfaces: "), 0, 3);

        className = new TextField();
        names.add(className, 1, 0);
        
        packageName = new TextField();
        names.add(packageName, 1, 1);
        
        parentClass = new ChoiceBox<>();
        parentClass.setConverter(new StringConverter<UMLObject>() {
            @Override
            public String toString(UMLObject uml) {
                if (uml== null)
                  return null;
                else 
                  return uml.getName();
            }

            @Override
            public UMLObject fromString(String string) {
                //NOT NEEDED
                return null;
            }
        });
        parentClass.setPrefWidth(150);
        names.add(parentClass, 1, 2);
        
        interfaceScrollPane = new ScrollPane();
        interfaceScrollPane.setMaxHeight(SCROLL_PANE_HEIGHT);
        interfaceScrollPane.setMaxWidth(150);
        interfaceList = new ListView<>();
        interfaceList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        interfaceList.setCellFactory((ListView<UMLObject> uml) -> {
            ListCell<UMLObject> cell = new ListCell<UMLObject>() {
                @Override
                protected void updateItem(UMLObject t, boolean bln) {
                    super.updateItem(t, bln);
                    if (t != null) {
                        setText(t.getName());
                    }
                }
                
            };
            return cell;
        });
        interfaceScrollPane.setContent(interfaceList);
        names.add(interfaceScrollPane, 1, 3);

        names.setVgap(10);
        componentToolbar.getChildren().add(names);

        varHeader = new HBox();
        varHeader.getChildren().add(new Label("Variables: "));
        addVarButton = gui.initChildButton(varHeader, ADD_ELEMENT_ICON.toString(), ADD_VAR_TOOLTIP.toString(), true);
        removeVarButton = gui.initChildButton(varHeader, REMOVE_ELEMENT_ICON.toString(), REMOVE_VAR_TOOLTIP.toString(), true);
        componentToolbar.getChildren().add(varHeader);

        varTable = new TableView<>();
        setupVarTable(varTable);
        varScrollPane = initScrollPane(varTable, componentToolbar);

        methodHeader = new HBox();
        methodHeader.getChildren().add(new Label("Methods: "));
        addMethodButton = gui.initChildButton(methodHeader, ADD_ELEMENT_ICON.toString(), ADD_METHOD_TOOLTIP.toString(), true);
        removeMethodButton = gui.initChildButton(methodHeader, REMOVE_ELEMENT_ICON.toString(), REMOVE_METHOD_TOOLTIP.toString(), true);
        methodHeader.getChildren().add(new Label("Args: "));
        addArgButton = gui.initChildButton(methodHeader, ADD_ELEMENT_ICON.toString(), ADD_ARG_TOOLTIP.toString(), true);
        removeArgButton = gui.initChildButton(methodHeader, REMOVE_ELEMENT_ICON.toString(), REMOVE_ARG_TOOLTIP.toString(), true);
        componentToolbar.getChildren().add(methodHeader);

        methodTable = new TableView<>();
        setupMethodTable(methodTable);
        methodScrollPane = initScrollPane(methodTable, componentToolbar);

        showPane = new HBox();
                showVars = new CheckBox();
                showMethods = new CheckBox();
        showPane.getChildren().addAll(new Label("Show Variables: "), showVars, new Label("Show Methods: "), showMethods);
        componentToolbar.getChildren().add(showPane);
        
        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(canvas);
        ((BorderPane) workspace).setRight(componentToolbar);
        createGridLines();
    }
        
    private void setupCanvasHandlers() {  
        CanvasController canvasControl = new CanvasController(app);
        canvas.setOnMousePressed(e-> {
           canvasControl.handleMousePressed(e.getX(), e.getY());
        });
        canvas.setOnMouseDragged(e-> {
            canvasControl.handleMouseDragged(e.getX(), e.getY());
        });
        canvas.setOnMouseReleased(e-> {
            canvasControl.handleMouseReleased(e.getX(), e.getY());
        });
        canvas.setOnMouseMoved(e -> {
            canvasControl.handleMouseMoved(e.getX(), e.getY());
        });
    }
    
    private void setupComponentHandlers() { 
        ComponentController componentControl = new ComponentController(app);
        className.setOnAction(e-> {
            componentControl.handleChangeClassName(packageName.getText(), className.getText());
        });
        packageName.setOnAction(e-> {
            componentControl.handleChangePackageName(packageName.getText(), className.getText());
        });
        
        parentClass.valueProperty().addListener(new ChangeListener<UMLObject>() {
            @Override public void changed(ObservableValue ov, UMLObject oldVal, UMLObject newVal) {
                if(activateControllers) 
                    componentControl.handleUpdateParent(newVal);
            }    
        });
        
        interfaceList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<UMLObject>() {
            @Override
            public void changed(ObservableValue ov, UMLObject oldVal, UMLObject newVal) {
                if (activateControllers) {
                        ObservableList<Integer> selectedListIndices = interfaceList.getSelectionModel().getSelectedIndices();
                    ObservableList<UMLObject> selectedList = interfaceList.getItems();
                    ArrayList<Integer> parentIDArray = new ArrayList<>();
                    for(Integer i : selectedListIndices) {
                        parentIDArray.add(selectedList.get(i).getID());
                    }
                    componentControl.handleUpdateParentInterfaces(parentIDArray);
                }
            }
        });
        
               
        addVarButton.setOnAction(e-> {
            componentControl.handleAddVar();
        });
        
        removeVarButton.setOnAction(e -> {
            if (varTable.getSelectionModel().getSelectedItem() != null) {
                componentControl.handleRemoveVar(varTable.getSelectionModel().getSelectedItem());
            }
        });
        
        varTable.setOnMouseClicked(e-> {
           if(e.getClickCount()==2 && varTable.getSelectionModel().getSelectedItem() != null)
               componentControl.handleEditVar(varTable.getSelectionModel().getSelectedItem());
        });
        
        addMethodButton.setOnAction(e-> {
            componentControl.handleAddMethod();
        });
        
        removeMethodButton.setOnAction(e -> {
            if (methodTable.getSelectionModel().getSelectedItem() != null) 
                componentControl.handleRemoveMethod(methodTable.getSelectionModel().getSelectedItem());
        });
        
        methodTable.setOnMouseClicked(e-> {
           if(e.getClickCount()==2 && methodTable.getSelectionModel().getSelectedItem() != null)
               componentControl.handleEditMethod(methodTable.getSelectionModel().getSelectedItem());
        });
        
        showVars.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
            componentControl.handleToggleVarShow(newVal);
        });
        
        showMethods.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
            componentControl.handleToggleMethodShow(newVal);
        });
        
        methodTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                addArgButton.setDisable(false);
                removeArgButton.setDisable(false);
            }
        });
        
        addArgButton.setOnAction(e -> {
            Method selectedMethod = methodTable.getSelectionModel().getSelectedItem();
            if (selectedMethod != null)
                componentControl.handleAddArg(selectedMethod);
        });
        
        removeArgButton.setOnAction(e -> {
            Method selectedMethod = methodTable.getSelectionModel().getSelectedItem();
            if (selectedMethod != null)
                componentControl.handleRemoveArg(selectedMethod);
        });
    }
    
    private void setupToolbarHandlers() {
        ToolbarController toolbarControl = new ToolbarController(app);
        gui.getCode().setOnAction(e -> {
            toolbarControl.handleExportCode();
        });
        gui.getPhoto().setOnAction(e -> {
            toolbarControl.handleExportPhoto();
        });
        gui.getSelect().setOnAction(e-> {
            toolbarControl.handleSelectTool();
            //app.getGUI().activateSelect(false);
        });
        gui.getResize().setOnAction(e-> {
            toolbarControl.handleResizeTool();
            //app.getGUI().activateSelect(true);
        });
        gui.getAddClass().setOnAction(e-> {
            toolbarControl.handleAddClassTool();
            //app.getGUI().activateSelect(true);
        });
        gui.getAddInterface().setOnAction(e-> {
            toolbarControl.handleAddInterfaceTool();
            //app.getGUI().activateSelect(true);
        });
        gui.getRemove().setOnAction(e-> {
            toolbarControl.handleRemove();
        });
        gui.getUndo().setOnAction(e-> {
            toolbarControl.handleUndo();
        });
        gui.getRedo().setOnAction(e-> {
            toolbarControl.handleRedo();
        });
        gui.getZoomIn().setOnAction(e -> {
            //toolbarControl.handleZoomIn();
        });
        gui.getZoomOut().setOnAction(e -> {
            toolbarControl.handleZoomOut();
        });
        gui.getGridToggle().selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
            toolbarControl.handleGridToggle(gui.getGridToggle().isSelected());
        });
        
        gui.getSnapToggle().selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
            toolbarControl.handleSnapToggle(gui.getGridToggle().isSelected());
        });
    }
    
    private void setupVarTable(TableView vars) {
        varNameCol = new TableColumn("Name");
        varNameCol.setCellValueFactory(f -> f.getValue().getSimpleName());
        //varNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        
        varTypeCol = new TableColumn("Type");
        varTypeCol.setCellValueFactory(f -> f.getValue().getSimpleType());
        //varNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        
        varStaticCol = new TableColumn("Static");
        varStaticCol.setCellValueFactory(f -> f.getValue().getSimpleStatic());
        varStaticCol.setCellFactory(tc ->new CheckBoxTableCell<>());
        
        varAccessCol = new TableColumn("Access");
        varAccessCol.setCellValueFactory(f -> f.getValue().getSimpleAccess());
        
        vars.getColumns().addAll(varNameCol, varTypeCol, varStaticCol, varAccessCol);
    }
    
    private void setupMethodTable(TableView methods) {
        methodNameCol = new TableColumn("Name");
        methodNameCol.setCellValueFactory(f -> f.getValue().getSimpleName());
        
        methodReturnCol = new TableColumn("Return");
        methodReturnCol.setCellValueFactory(f -> f.getValue().getSimpleReturn());
        
        methodStaticCol = new TableColumn("Static");
        methodStaticCol.setCellValueFactory(f -> f.getValue().getSimpleStatic());
        methodStaticCol.setCellFactory(tc ->new CheckBoxTableCell<>());
        
        methodAbstractCol = new TableColumn("Abstract");
        methodAbstractCol.setCellValueFactory(f -> f.getValue().getSimpleAbstract());
        methodAbstractCol.setCellFactory(tc ->new CheckBoxTableCell<>());
        
        methodAccessCol = new TableColumn("Access");
        methodAccessCol.setCellValueFactory(f -> f.getValue().getSimpleAccess());
        
        //NEED TO FIX ARGS STUFF
        methodArgsCol = new TableColumn("Arguments");
        
        argNameCols = FXCollections.observableArrayList();
        argTypeCols = FXCollections.observableArrayList();
            
        methods.getColumns().addAll(methodNameCol, methodReturnCol, methodStaticCol, 
                methodAbstractCol, methodAccessCol, methodArgsCol);
    }
    
    private void addArgCols (UMLObject selected) {
        argNameCols.clear();
        argTypeCols.clear();
        methodArgsCol.getColumns().clear();
        int maxArgNum = 0;
        HashMap<String,Method> methodMap = selected.getMethodMap();
        Set<String> keys = methodMap.keySet(); 
        for (String key: keys) {
            Method method = methodMap.get(key);
            HashMap<String,String> argMap = method.getArgMap();
            if (argMap.size() > maxArgNum) {
                int numColToAdd = argMap.size() - maxArgNum;
                for (int i=1; i<=numColToAdd; i++) {
                    TableColumn argName = new TableColumn("ArgName");
                    TableColumn argType = new TableColumn("ArgType");
                    methodArgsCol.getColumns().addAll(argName, argType);
                    
                    argNameCols.add(argName);
                    argTypeCols.add(argType);
                }
                maxArgNum = method.getArgMap().size();
            }
            //NEED TO POPULATE ARG COLS LATER
        }
    }
    
    private ScrollPane initScrollPane (TableView table, VBox parentPane) {
            ScrollPane scrollPane = new ScrollPane();
            scrollPane.setContent(table);
            scrollPane.setPannable(true);
            scrollPane.setPrefViewportWidth(SCROLL_PANE_WIDTH);
            //scrollPane.setPrefViewportHeight(SCROLL_PANE_HEIGHT);
            parentPane.getChildren().add(scrollPane);
            return scrollPane;
    }
    
    private void createGridLines () {
        lines = new ArrayList<>();
        DataManager dataM = (DataManager) app.getDataComponent();
        if (dataM.getShowGrid()) {
            int paneWidth = (int) app.getGUI().getWindow().getWidth();
            int paneHeight = (int) app.getGUI().getWindow().getHeight();
            int zoom = dataM.getGridWidth();

            //VERT LINES
            for (int i=0; i<paneWidth; i+=zoom) {
                Line line = new Line (i,0,i,paneHeight);
                line.setStroke(Color.LIGHTSKYBLUE);
                canvas.getChildren().add(line);
                lines.add(line);
            }
            //HORZ LINES
            for (int i=0; i<paneHeight; i+=zoom) {
                Line line = new Line (0,i,paneWidth,i);
                line.setStroke(Color.LIGHTSKYBLUE);
                canvas.getChildren().add(line);
                lines.add(line);
            }
        }
    }
    
    public Pane getCanvas() {
        return canvas;
    }

    public void loadSelectedUMLSettings(UMLObject umlObj) {
        if (activateControllers) {
        activateControllers = false;
        className.setText(umlObj.getName());
        className.setDisable(false);
        packageName.setText(umlObj.getPackageName());
        packageName.setDisable(false);

        DataManager dataM = (DataManager) app.getDataComponent();
        ArrayList<UMLObject> umls = dataM.getUMLObjs();

        //GET ALL POSSIBLE PARENT INTERFACES
        interfaceList.getItems().clear();
        interfaceList.setDisable(false);
        for (UMLObject uml : umls) {
            if (uml.getIsInterface() && !uml.equals(umlObj)) {
                interfaceList.getItems().add(uml);
            }
        }
        //DISPLAY ALL SELECTED
        ArrayList<UMLObject> interfaces  = umlObj.getInterfaceArray(dataM);
        for (UMLObject i: interfaces) {
            interfaceList.getSelectionModel().select(i);
        }
        
        //SET TOGGLE FOR SHOWVARS AND SHOWMETHODS
        showVars.setSelected(umlObj.getShowVars());
        showMethods.setSelected(umlObj.getShowMethods());

        // IF IT IS AN INTERFACE, DISABLE VARIABLE CONTROL
        if (umlObj.getIsInterface()) {
            parentClass.setDisable(true);
            varTable.setDisable(true);
            showVars.setDisable(true);
            addVarButton.setDisable(true);
            removeVarButton.setDisable(true);
        } 
        else {
            //GET ALL POSSIBLE PARENT CLASSES
            parentClass.setDisable(false);
            parentClass.getItems().clear();
            parentClass.getItems().add(null);
            for (UMLObject uml : umls) {
                if (!uml.getIsInterface() && !uml.equals(umlObj)) 
                    parentClass.getItems().add(uml);      
            }
            if (umlObj.getParentID() != -1) {
                UMLObject parent = umlObj.getParentClass(dataM);
                parentClass.setValue(parent);
            } 

            
            //VARIABLE CONTROLS
            varTable.setDisable(false);
            showVars.setDisable(false);
            addVarButton.setDisable(false);
            if (umlObj.getVarMap().size() > 0) {
                removeVarButton.setDisable(false);
            } else if (umlObj.getVarMap().size() <= 0) {
                removeVarButton.setDisable(true);
            }
        }
        //UPDATE THE VARS
        varData.clear();
        HashMap<String, Variable> varMap = umlObj.getVarMap();
        Set<String> varKeys = varMap.keySet();
        for (String key : varKeys) {
            Variable var = varMap.get(key);
            varData.add(var);
        }
        varTable.setItems(varData);

        //METHOD CONTROLS
        methodTable.setDisable(false);
        showMethods.setDisable(false);
        addMethodButton.setDisable(false);
        if (umlObj.getMethodMap().size() > 0) {
            removeMethodButton.setDisable(false);
        } else if (umlObj.getVarMap().size() <= 0) {
            removeMethodButton.setDisable(true);
        }

        //UPDATE THE METHODS
        methodData.clear();
        HashMap<String, Method> methodMap = umlObj.getMethodMap();
        Set<String> methodKeys = methodMap.keySet();
        for (String key : methodKeys) {
            Method method = methodMap.get(key);
            methodData.add(method);
        }
        methodTable.setItems(methodData);
        addArgCols(umlObj);
        activateControllers = true;
        }
    }
    
    public void loadNoSelectedUMLSetting () {
        activateControllers = false;
        String noUMLString = "";
        className.setText(noUMLString);
        className.setDisable(true);
        
        packageName.setText(noUMLString);
        packageName.setDisable(true);
        
        parentClass.getItems().clear();
        parentClass.setDisable(true);
        
        interfaceList.getItems().clear();
        interfaceList.setDisable(true);
        
        varData.clear();
        varTable.setDisable(true);
        addVarButton.setDisable(true);
        removeVarButton.setDisable(true);
        showVars.setDisable(true);
        showVars.setSelected(false);
        
        methodData.clear();
        methodTable.setDisable(true);
        addMethodButton.setDisable(true);
        removeMethodButton.setDisable(true);
        addArgButton.setDisable(true);
        removeArgButton.setDisable(true);
        showMethods.setDisable(true);
        showMethods.setSelected(false);
        activateControllers = true;
    }
    
    public void showLines () {
        for (Line l: lines)
            l.setStroke(Color.LIGHTSKYBLUE);
    }
    
    public void hideLines () {
        for (Line l: lines)
            l.setStroke(Color.WHITE);
    }
    
    public TextField getClassTextField() {
        return className;
    }
    
    public TextField getPackageTextField() {
        return packageName;
    }
    
    public ChoiceBox getParentClassChoice () {
        return parentClass;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.control;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.TreeItem;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jcd.data.DataManager;
import jcd.data.JavaPackage;
import jcd.data.UMLObject;
import jcd.data.jClassStates;
import jcd.gui.Workspace;
import saf.AppTemplate;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author haley.he
 */
public class ToolbarController {
    AppTemplate app;
    
    public ToolbarController (AppTemplate initApp){
        app = initApp;
    }
   

    public void handleExportPhoto() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Take a Snapshot");
        fc.setInitialDirectory(new File(System.getProperty("user.dir")));
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("png", "png"));
        File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
        try {
            if (selectedFile != null) {
                File photo = new File(selectedFile.getAbsoluteFile() + ".png");
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                Pane canvas = workspace.getCanvas();
                WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", photo);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void handleExportCode() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        try {
            DirectoryChooser dc = new DirectoryChooser();
            dc.setTitle("Export to Java Source Code (Creates a src Folder");
            dc.setInitialDirectory(new File(System.getProperty("user.dir")));
            File selectedFile = dc.showDialog(app.getGUI().getWindow());
            if (selectedFile != null) {
                DataManager dataM = (DataManager) app.getDataComponent();
                JavaPackage src = (JavaPackage) dataM.getProject().getValue();
                generateSourceCode(dataM.getProject(), selectedFile.getAbsolutePath());
                dialog.show("Export Completed", "The diagam has successfully been exported to Java source code.");
            }
        } catch (IOException ex) {
            dialog.show("Export Fail", "Code cannot be exported. Please make sure everthing is labeled properly.");
        }
    }

    public void handleSelectTool() {
        DataManager dataM = (DataManager) app.getDataComponent();
        dataM.setCurrentState(jClassStates.SELECT);
        //System.out.println(dataM.getCurrentState().toString());
    }

    public void handleResizeTool() {
                DataManager dataM = (DataManager) app.getDataComponent();
        dataM.setCurrentState(jClassStates.RESIZE);
        //System.out.println(dataM.getCurrentState().toString());
    }

    public void handleAddClassTool() {
                DataManager dataM = (DataManager) app.getDataComponent();
        dataM.setCurrentState(jClassStates.ADD_CLASS);
        //System.out.println(dataM.getCurrentState().toString());
    }

    public void handleAddInterfaceTool() {
                DataManager dataM = (DataManager) app.getDataComponent();
        dataM.setCurrentState(jClassStates.ADD_INTERFACE);
        //System.out.println(dataM.getCurrentState().toString());
    }

    public void handleRemove() {
        DataManager dataM = (DataManager) app.getDataComponent();
        dataM.removeUML();
    }

    public void handleUndo() {
        DataManager dataM = (DataManager) app.getDataComponent();
        ArrayList<UMLObject> umls = dataM.getUMLObjs();
        for (UMLObject uml : umls) {
            System.out.print(uml.getID() + ") " + uml.getName() + ": " );
            ArrayList<UMLObject> inters = uml.getInterfaceArray(dataM);
            for (UMLObject i : inters)
                System.out.print(i.getName() + " ");
            
            System.out.println();
        }
        System.out.println();
    }

    public void handleRedo() {

    }

    public void handleZoomIn() {
        /*
        int currentZoom = dataM.getZoomAmount();
              System.out.println(currentZoom);
        if (currentZoom <=  90) {
            dataM.setZoomAmount(currentZoom+10);
            if (workspace.getCanvas().getChildren()!= null)
                workspace.getCanvas().getChildren().clear(); //NEED TO FIX TO BECOME REMOVE NOT CLEAR
            workspace.reloadWorkspace();
        }
        */
    }

    public void handleZoomOut() {
        /*
        int currentZoom = dataM.getZoomAmount();
        System.out.println(currentZoom);
        if (currentZoom >= 30) {
            dataM.setZoomAmount(currentZoom-10);
            if (workspace.getCanvas().getChildren()!= null)
                workspace.getCanvas().getChildren().clear(); //NEED TO FIX TO BECOME REMOVE NOT CLEAR
            workspace.reloadWorkspace();
        }
        */
    }

    public void handleGridToggle(boolean checked) {
        DataManager dataM = (DataManager) app.getDataComponent();
            dataM.setShowGrid(checked);
            
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if (checked)
           workspace.showLines();
        else 
            workspace.hideLines();
    }

    public void handleSnapToggle(boolean checked) {
        DataManager dataM = (DataManager) app.getDataComponent();
        dataM.setEnforceSnap(checked);
    }

    private void generateSourceCode (TreeItem node, String pathName) throws IOException {
        JavaPackage nodeData = (JavaPackage) node.getValue();
        String subPackName = nodeData.getPackageName();
        if (subPackName.contains("."))
            subPackName = subPackName.substring(subPackName.lastIndexOf(".")+1);
        String newPathName = pathName + "\\" + subPackName + "\\";
        new File(newPathName).mkdir();

        ObservableList children = node.getChildren();
        for (int i = 0; i < children.size(); i++) {
            TreeItem child = (TreeItem) children.get(i);
            if (child.getValue() instanceof JavaPackage) {
                JavaPackage jPack = (JavaPackage) child.getValue();
                String packName = jPack.getPackageName();
                if (packName.equals("java") || packName.equals("javafx")) {
                    generateJavaImports();
                } else {
                    generateSourceCode(child, newPathName);
                }
            } else if (child.getValue() instanceof UMLObject) {
                UMLObject uml = (UMLObject) child.getValue();
                String umlName = uml.getName();
                String javaFileName = newPathName + umlName + ".java";
                File javaFile = new File(javaFileName);
                javaFile.getParentFile().mkdirs();
                javaFile.createNewFile();
                writeJavaCode(uml, javaFileName);
            }
        }
    }
    
    private void generateSourceCode(TreeItem node, String pathName, String packageName) throws IOException {
        String newPathName = pathName + "/" + packageName + "/";
        new File(newPathName).mkdir();

        ObservableList children = node.getChildren();
        for (int i = 0; i < children.size(); i++) {
            TreeItem child = (TreeItem) children.get(i);
            if (child.getValue() instanceof JavaPackage) {
                JavaPackage jPack = (JavaPackage) child.getValue();
                String packName = jPack.getPackageName();
                if (packName.equals("java") || packName.equals("javafx")) {
                    generateJavaImports();
                } else {
                    generateSourceCode(child, newPathName, packName);
                }
            } else if (child.getValue() instanceof UMLObject) {
                UMLObject uml = (UMLObject) child.getValue();
                String umlName = uml.getName();
                String javaFileName = newPathName + umlName + ".java";
                File javaFile = new File(javaFileName);
                javaFile.getParentFile().mkdirs();
                javaFile.createNewFile();
                writeJavaCode(uml, javaFileName);
            }
        }
    }
    
    private void writeJavaCode (UMLObject uml, String filePath) throws IOException {
                DataManager dataM = (DataManager) app.getDataComponent();
        PrintWriter writer = new PrintWriter (filePath);
        writer.print(uml.exportToCode(dataM));
        writer.close();
    }
    
    private void generateJavaImports() {
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.control;

import javafx.scene.Cursor;
import javafx.scene.Scene;
import jcd.data.DataManager;
import jcd.data.UMLObject;
import static jcd.data.jClassStates.*;
import jcd.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author haley.he
 */
public class CanvasController {
    AppTemplate app;

    
    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }
    
    public void handleMouseMoved(double x, double y) {
        DataManager dataM = (DataManager) app.getDataComponent();
        Scene scene = app.getGUI().getPrimaryScene();
        if (dataM.isInState(RESIZE)) {
            UMLObject selected = dataM.getSelectedUML();
            if (selected != null) {
                double selectedWPos = selected.getX() + selected.getWidth();
                double selectedHPos = selected.getY() + selected.getHeight();
                if (x >= (selectedWPos-5) && x <= (selectedWPos+5) && 
                        y > selected.getY() && y < selectedHPos) 
                    scene.setCursor(Cursor.H_RESIZE);
                else if (y >= (selectedHPos-5) && y <= (selectedHPos+5) && 
                        x > selected.getX() && x < selectedWPos)
                    scene.setCursor(Cursor.V_RESIZE);
                else 
                    scene.setCursor(Cursor.DEFAULT);
            }
        }
    }
    
    public void handleMousePressed (double x, double y) {
        DataManager dataM = (DataManager) app.getDataComponent();
        Workspace work = (Workspace) app.getWorkspaceComponent();
        Scene scene = app.getGUI().getPrimaryScene();

        if (dataM.isInState(SELECT)) {
            UMLObject selected = dataM.selectTopUMLObj(x, y);
            if (selected != null) {
                scene.setCursor(Cursor.MOVE);
                app.getGUI().activateEditControl(true);
                dataM.setCurrentState(DRAG_UML);
            }
            else {
                scene.setCursor(Cursor.DEFAULT);
                app.getGUI().activateEditControl(false);
                dataM.setCurrentState(DRAG_NOTHING);
                work.loadNoSelectedUMLSetting();
            }
        }
        else if (dataM.isInState(RESIZE)) {
            if (scene.getCursor() == Cursor.H_RESIZE) 
                dataM.setCurrentState(RESIZE_UML_H);
            else if (scene.getCursor() == Cursor.V_RESIZE) 
                dataM.setCurrentState(RESIZE_UML_V);
            else
                dataM.setCurrentState(RESIZE_NOTHING);
        }
        else if (dataM.isInState(ADD_CLASS)) {
            dataM.addClassUML(x, y);
            app.getGUI().activateEditControl(true);
                        app.getGUI().updateToolbarControls(false);
        }
        else if (dataM.isInState(ADD_INTERFACE)) {
            dataM.addInterfaceUML(x, y);
            app.getGUI().activateEditControl(true);
                        app.getGUI().updateToolbarControls(false);
        } 
    }
    
    public void handleMouseDragged (double x, double y) {
        DataManager dataM = (DataManager) app.getDataComponent();
        if (dataM.isInState(DRAG_UML)) {
            UMLObject umlToDrag = dataM.getSelectedUML();
            if (dataM.getEnforceSnap())
                umlToDrag.drag(x, y, dataM.getGridWidth());
            else 
                umlToDrag.drag(x, y);
            app.getGUI().updateToolbarControls(false);
        }
        else if (dataM.isInState(RESIZE_UML_H)) {
            UMLObject umlToResize = dataM.getSelectedUML();
            umlToResize.sizeW(x);
            umlToResize.updateUMLContent();
            app.getGUI().updateToolbarControls(false);
        }
        else if (dataM.isInState(RESIZE_UML_V)) {
            UMLObject umlToResize = dataM.getSelectedUML();
            umlToResize.sizeH(y);
            umlToResize.updateUMLContent();
            app.getGUI().updateToolbarControls(false);
        }
    };

    public void handleMouseReleased(double x, double y) {
        DataManager dataM = (DataManager) app.getDataComponent();

        if (dataM.isInState(RESIZE_UML_H) || dataM.isInState(RESIZE_UML_V)) {
	    dataM.setCurrentState(RESIZE);
	    app.getGUI().updateToolbarControls(false);
	}
	else if (dataM.isInState(DRAG_UML) || dataM.isInState(DRAG_NOTHING)) {
	    dataM.setCurrentState(SELECT);
	    app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
	    app.getGUI().updateToolbarControls(false);
	}
    }
        
    
}

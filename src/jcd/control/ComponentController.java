/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import static jcd.PropertyType.*;
import jcd.data.DataManager;
import jcd.data.Method;
import jcd.data.UMLObject;
import jcd.data.Variable;
import jcd.gui.Workspace;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author haley.he
 */
public class ComponentController  {
    AppTemplate app;
    AppMessageDialogSingleton changeErrorMessage;
    ObservableList<String> accessMods;
    
    public ComponentController(AppTemplate initApp) {
        app = initApp;
        accessMods = FXCollections.observableArrayList(
                "public",
                "private",
                "protected",
                "default");
    }
    
    public void handleChangeClassName (String packageName, String newClassName) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataM = (DataManager) app.getDataComponent();
        if (dataM.getSelectedUML() != null) {
            UMLObject selectedUML = dataM.getSelectedUML();
            //if there is no change to the name, do nothing
            if (selectedUML.getName().equals(newClassName))
                return;
            
            String [] packageParse = packageName.toLowerCase().split("\\.");
            //if (dataM.searchPackagesForClass(dataM.getProject(),packageParse, newClassName, 0)) {
            if (dataM.searchPackagesForClass2(packageName, newClassName)) {
                //show error message
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(CLASS_NAME_UPATE_ERROR_TITLE), props.getProperty(CLASS_NAME_UPATE_ERROR_MESSAGE));
                
                //change name display on component toolbar back to the class's old class name
                workspace.getClassTextField().setText(selectedUML.getName());
                return;
            }
            HashMap<String,UMLObject> umlMap = dataM.getUMLmap();
            umlMap.remove(selectedUML.getName(), selectedUML);
            selectedUML.setName(newClassName);
            umlMap.put(selectedUML.getName(), selectedUML);
            selectedUML.updateUMLContent();
            workspace.loadSelectedUMLSettings(selectedUML);
        }
    }

    public void handleChangePackageName(String newPackageName, String className) {
        DataManager dataM = (DataManager) app.getDataComponent();
                        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if (dataM.getSelectedUML() != null) {
            UMLObject selectedUML = dataM.getSelectedUML();
            //if there is no change to the name, do nothing
            if (selectedUML.getPackageName().equals(newPackageName))
                return;
            
            
            //check to see if the packageExist
            dataM.addPackage(newPackageName);
            
            if (dataM.searchPackagesForClass2 (newPackageName, className)) {
                //show error message
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(PACKAGE_NAME_UPATE_ERROR_TITLE), props.getProperty(PACKAGE_NAME_UPATE_ERROR_MESSAGE));
                
                //change name display on component toolbar back to the class's old package name
                workspace.getPackageTextField().setText(selectedUML.getPackageName());
                return;
            }
            
            //remove the UML from the old package
            dataM.removeUMLFromPackage(selectedUML.getPackageName(), selectedUML);
            //add it to the new package
            dataM.addUMLToPackage(newPackageName, selectedUML);
            
            //update UML
            selectedUML.setPackageName(newPackageName);
            selectedUML.updateUMLContent();
             workspace.loadSelectedUMLSettings(selectedUML);
        }
    }
    
    public void handleAddVar() {
        Variable newVar = new Variable();
        newVar = showVarDialog(newVar, ADD_VAR_TITLE.toString(), VAR_HEADER_MESSAGE.toString(), ADD_BUTTON.toString());
        if (newVar != null) {
            DataManager dataM = (DataManager) app.getDataComponent();
            UMLObject selected = dataM.getSelectedUML();
            HashMap<String, Variable> varMap = selected.getVarMap();
            varMap.put(newVar.getName(), newVar);
            
            selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.reloadLines();
            workspace.loadSelectedUMLSettings(selected);
        }      
    }
    
    public void handleEditVar(Variable var) {
        Variable varToEdit = showVarDialog(var, EDIT_VAR_TITLE.toString(), VAR_HEADER_MESSAGE.toString(), EDIT_BUTTON.toString());
        if (varToEdit != null ) { 
            DataManager dataM = (DataManager) app.getDataComponent();
            UMLObject selected = dataM.getSelectedUML();
            HashMap<String, Variable> varMap = selected.getVarMap();
            varMap.remove(var.getName());
            varMap.put(varToEdit.getName(), varToEdit);
            
             selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.loadSelectedUMLSettings(selected);
            workspace.reloadLines();
        }      
    }
    
    public void handleRemoveVar(Variable varToRemove) {
        DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();
        HashMap<String, Variable> varMap = selected.getVarMap();
        varMap.remove(varToRemove.getName());

            
            selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.reloadLines();
            workspace.loadSelectedUMLSettings(selected);   
    }
    
    public void handleAddMethod() {
        Method newMethod= new Method();
        newMethod = showMethodDialog(newMethod, ADD_METHOD_TITLE.toString(), METHOD_HEADER_MESSAGE.toString(), ADD_BUTTON.toString());
        if (newMethod!= null) {
            DataManager dataM = (DataManager) app.getDataComponent();
            UMLObject selected = dataM.getSelectedUML();
            HashMap<String, Method> methodMap = selected.getMethodMap();
            methodMap.put(newMethod.getName(), newMethod);
           
            selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.reloadLines();
            workspace.loadSelectedUMLSettings(selected);
        }      
    }
    
    public void handleEditMethod(Method method) {
        HashMap <String,String> methodArgs = method.getArgMap();
        Method newMethod = showMethodDialog(method, EDIT_METHOD_TITLE.toString(), METHOD_HEADER_MESSAGE.toString(), EDIT_BUTTON.toString());
        if (newMethod != null) {
            DataManager dataM = (DataManager) app.getDataComponent();
            UMLObject selected = dataM.getSelectedUML();
            HashMap<String, Method> methodMap = selected.getMethodMap();
            methodMap.remove(method.getName());
            methodMap.put(newMethod.getName(), newMethod);
            newMethod.setArgMap(methodArgs);

             selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.reloadLines();
            workspace.loadSelectedUMLSettings(selected);
        }
    }

    public void handleRemoveMethod(Method methodToRemove) {
        DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();
        HashMap<String, Method> methodMap = selected.getMethodMap();
        methodMap.remove(methodToRemove.getName());

            selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.reloadLines();
            workspace.loadSelectedUMLSettings(selected);
    }
    
    public void handleToggleVarShow (boolean show) {
        DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();
        if (selected != null ) {
        selected.setShowVars(show);
        selected.updateUMLContent();
                
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.loadSelectedUMLSettings(selected);
        }
    }
    
    public void handleToggleMethodShow (boolean show) {
        DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();
        if (selected != null) {
        selected.setShowMethods(show);
        selected.updateUMLContent();
        
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.loadSelectedUMLSettings(selected);
        }
    }
    
    public void handleAddArg (Method method) {
        showAddArgDialog(method, ADD_ARG_TITLE.toString(), ADD_ARG_MESSAGE.toString(), ADD_BUTTON.toString());
        
        DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();
        
             selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.reloadLines();
            workspace.loadSelectedUMLSettings(selected);
    }
    
    public void handleRemoveArg (Method method) {
        showRemoveArgDialog(method, REMOVE_ARG_TITLE.toString(), REMOVE_ARG_MESSAGE.toString(), REMOVE_BUTTON.toString());
                    DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();
        
              selected.updateUMLContent();
            dataM.removeConnectingRelation(selected);
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.reloadLines();
            workspace.loadSelectedUMLSettings(selected);
    }
    
    private Variable showVarDialog(Variable selectedVar, String title, String header, String buttonText) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //init the dialog and basic text
        Dialog<Variable> varDialog = new Dialog<>();
        varDialog.setTitle(props.getProperty(title));
        varDialog.setHeaderText(props.getProperty(header));

        //INIT LABELS AND FIELDS
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField varName = new TextField();
        varName.setText(selectedVar.getName());
        TextField varType = new TextField();
        varType.setText(selectedVar.getType());
        CheckBox varStatic = new CheckBox();
        varStatic.setSelected(selectedVar.getIsStatic());
        ChoiceBox<String> varAccess = new ChoiceBox<> ();
        varAccess.setItems(accessMods);
        varAccess.setValue(selectedVar.getAccessString());
        grid.add(new Label("Variable Name"), 0, 0);
        grid.add(varName, 1, 0);
        grid.add(new Label("Variable Type"), 0, 1);
        grid.add(varType, 1, 1);
        grid.add(new Label ("Variable Static Property"), 0, 2);
        grid.add(varStatic, 1, 2);
        grid.add(new Label ("Variable Access Modifier"), 0, 3);
        grid.add(varAccess, 1, 3);
        varDialog.getDialogPane().setContent(grid);
        
        //INIT THE BUTTONS
        ButtonType saveButtonType = new ButtonType(props.getProperty(buttonText), ButtonData.OK_DONE);
        varDialog.getDialogPane().getButtonTypes().addAll(saveButtonType, ButtonType.CANCEL);

   
        varDialog.setResultConverter(dialogButton -> {
            if (dialogButton == saveButtonType) {
                return new Variable(varName.getText(), 
                        varType.getText(), 
                        varAccess.getSelectionModel().getSelectedItem(), 
                        varStatic.isSelected());
            }
            return null;
        });
        
        Optional<Variable> result = varDialog.showAndWait();
        
        if (result.isPresent()) 
            return result.get();
        else 
            return null;
    }
    
    private Method showMethodDialog(Method selectedMethod, String title, String header, String buttonText) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //init the dialog and basic text
        Dialog<Method> methodDialog = new Dialog<>();
        methodDialog.setTitle(props.getProperty(title));
        methodDialog.setHeaderText(props.getProperty(header));

        //INIT LABELS AND FIELDS
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField methodName = new TextField();
            methodName.setText(selectedMethod.getName());
        TextField methodReturn = new TextField();
            methodReturn.setText(selectedMethod.getReturnType());
        CheckBox methodStatic = new CheckBox();
            methodStatic.setSelected(selectedMethod.getIsStatic());
        CheckBox methodAbstract = new CheckBox();
            methodAbstract.setSelected(selectedMethod.getIsAbstract());
        ChoiceBox<String> methodAccess = new ChoiceBox<> ();
            methodAccess.setItems(accessMods);
            methodAccess.setValue(selectedMethod.getAccessString());
        
        grid.add(new Label("Method Name"), 0, 0);
        grid.add(methodName, 1, 0);
        grid.add(new Label("Method Return Type"), 0, 1);
        grid.add(methodReturn, 1, 1);
        grid.add(new Label ("Method Static Property"), 0, 2);
        grid.add(methodStatic, 1, 2);
        grid.add(new Label ("Method Abstract Property"), 0, 3);
        grid.add(methodAbstract, 1, 3);
        grid.add(new Label ("Method Acess Modifier"), 0, 4);
        grid.add(methodAccess, 1, 4);
        methodDialog.getDialogPane().setContent(grid);
        
        //INIT THE BUTTONS
        ButtonType saveButtonType = new ButtonType(props.getProperty(buttonText), ButtonData.OK_DONE);
        methodDialog.getDialogPane().getButtonTypes().addAll(saveButtonType, ButtonType.CANCEL);

   
        methodDialog.setResultConverter(dialogButton -> {
            if (dialogButton == saveButtonType) {
                return new Method (methodName.getText(), 
                        methodReturn.getText(), 
                        methodAccess.getSelectionModel().getSelectedItem(), 
                        methodStatic.isSelected(),
                        methodAbstract.isSelected());
            }
            return null;
        });
        
        Optional<Method> result = methodDialog.showAndWait();
        
        if (result.isPresent()) 
            return result.get();
        else 
            return null;
    }
    
    private void showAddArgDialog(Method selectedMethod, String title, String header, String buttonText) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //init the dialog and basic text
        Dialog<ArrayList<String>> argDialog = new Dialog<>();
        argDialog.setTitle(props.getProperty(title));
        argDialog.setHeaderText(props.getProperty(header));

        //INIT LABELS AND FIELDS
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField argName = new TextField();
        argName.setPromptText("Argument name");
        TextField argType = new TextField();
        argType.setPromptText("Argument type");
        grid.add(new Label("Argument Name"), 0, 0);
        grid.add(argName, 1, 0);
        grid.add(new Label("Argument Type"), 0, 1);
        grid.add(argType, 1, 1);
        argDialog.getDialogPane().setContent(grid);
        
        //INIT THE BUTTONS
        ButtonType saveButtonType = new ButtonType(props.getProperty(buttonText), ButtonData.OK_DONE);
        argDialog.getDialogPane().getButtonTypes().addAll(saveButtonType, ButtonType.CANCEL);

   
        argDialog.setResultConverter(dialogButton -> {
            if (dialogButton == saveButtonType) {
                ArrayList<String> arg = new ArrayList<>();
                if (argName.getText() != null)
                    arg.add(argName.getText());
                if (argType.getText() != null)
                    arg.add(argType.getText());
                return arg;
            }
            return null;
        });
        
        Optional<ArrayList<String>> result = argDialog.showAndWait();
        
        if (result.isPresent() && result.get().size() == 2) {
            ArrayList<String> resultArray = result.get();
            selectedMethod.addArg(resultArray.get(0), resultArray.get(1));
        }
    }
    
    private void showRemoveArgDialog(Method selectedMethod, String title, String header, String buttonText) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //init the dialog and basic text
        Dialog<String> argDialog = new Dialog<>();
        argDialog.setTitle(props.getProperty(title));
        argDialog.setHeaderText(props.getProperty(header));

        //INIT LABELS AND FIELDS
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField argName = new TextField();
        argName.setPromptText("Argument name");
        grid.add(new Label("Argument Name"), 0, 0);
        grid.add(argName, 1, 0);
        argDialog.getDialogPane().setContent(grid);
        
        //INIT THE BUTTONS
        ButtonType saveButtonType = new ButtonType(props.getProperty(buttonText), ButtonData.OK_DONE);
        argDialog.getDialogPane().getButtonTypes().addAll(saveButtonType, ButtonType.CANCEL);

        argDialog.setResultConverter(dialogButton -> {
            if (dialogButton == saveButtonType) {
                return argName.getText();
            }
            return null;
        });
        
        Optional<String> result = argDialog.showAndWait();
        
        if (result.isPresent()) {
            if (result != null)
                selectedMethod.removeArg(result.get());
        }
    }
    
    public void handleUpdateParent(UMLObject parent) {
        DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();      
        dataM.removeConnectingRelation(selected);
        if (parent != null) {
            ArrayList<UMLObject> umls = dataM.getUMLObjs();
            for (UMLObject uml : umls) {
                if (uml.equals(parent)) {
                    selected.setParentClass(uml);
                    selected.updateUMLContent();
                    Workspace workspace = (Workspace) app.getWorkspaceComponent();
                    workspace.reloadLines();
                    workspace.loadSelectedUMLSettings(selected);
                    return;
                }
            }
        }
        else {
            selected.setParentID(-1);
        }
    }

    public void handleUpdateParentInterfaces(ArrayList<Integer> selectedIndices) {
        DataManager dataM = (DataManager) app.getDataComponent();
        UMLObject selected = dataM.getSelectedUML();
        dataM.removeConnectingRelation(selected);
        
        ArrayList<Integer> ids = selected.getInterfaceIDs();
        ids.clear();
        for (int i=0; i<selectedIndices.size(); i++) 
            ids.add(selectedIndices.get(i));
        
        selected.updateUMLContent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
                workspace.reloadLines();
        workspace.loadSelectedUMLSettings(selected);
    }
}

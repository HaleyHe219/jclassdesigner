/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd;

/**
 *
 * @author haley.he
 */
public enum PropertyType {
    ADD_ELEMENT_ICON,
    REMOVE_ELEMENT_ICON,
    ADD_VAR_TOOLTIP,
    REMOVE_VAR_TOOLTIP,
    ADD_METHOD_TOOLTIP,
    REMOVE_METHOD_TOOLTIP,
        ADD_ARG_TOOLTIP,
    REMOVE_ARG_TOOLTIP,
    
    ADD_VAR_TITLE,
    EDIT_VAR_TITLE,
    VAR_HEADER_MESSAGE,
    ADD_METHOD_TITLE,
    EDIT_METHOD_TITLE,
    METHOD_HEADER_MESSAGE,
    ADD_BUTTON,
    EDIT_BUTTON,
    REMOVE_BUTTON,
    ADD_ARG_TITLE,
    ADD_ARG_MESSAGE,
    REMOVE_ARG_TITLE,
    REMOVE_ARG_MESSAGE,
    
    CLASS_NAME_UPATE_ERROR_TITLE,
    CLASS_NAME_UPATE_ERROR_MESSAGE,
    PACKAGE_NAME_UPATE_ERROR_TITLE,
    PACKAGE_NAME_UPATE_ERROR_MESSAGE

}

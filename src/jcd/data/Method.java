/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author haley.he
 */
public class Method {
    SimpleStringProperty name;
    SimpleStringProperty returnType;
    AccessType access;
    SimpleStringProperty accessString;
    SimpleBooleanProperty isStatic;
    SimpleBooleanProperty isAbstract;
    HashMap<String,String> args;
    
    public Method () {
        name = new SimpleStringProperty("methodName");
        returnType = new SimpleStringProperty("void");
        access = AccessType.PUBLIC;
        if (access != AccessType.DEFAULT)
            accessString = new SimpleStringProperty(access.getWord());
        else 
            accessString = new SimpleStringProperty("default");
        isStatic = new SimpleBooleanProperty(false);
        isAbstract = new SimpleBooleanProperty(false);
        args = new HashMap<> ();
    }
    
    public Method (String name, String returnType, String access, boolean isStatic, boolean isAbstract)  {
        this.name = new SimpleStringProperty(name);
        this.returnType = new SimpleStringProperty(returnType);
        if (access.equals("public")) 
            this.access = AccessType.PUBLIC;
        else if (access.equals("private"))
            this.access = AccessType.PRIVATE;
        else if (access.equals("protected"))
            this.access = AccessType.PROTECTED;
        else 
            this.access = AccessType.DEFAULT;
        
        if (this.access != AccessType.DEFAULT)
            accessString = new SimpleStringProperty(this.access.getWord());
        else 
            accessString = new SimpleStringProperty("default");
        
        this.isStatic = new SimpleBooleanProperty(isStatic);
        this.isAbstract = new SimpleBooleanProperty(isAbstract);
        args = new HashMap<> ();
    }
    
    public String getName () {return name.get();}
    public SimpleStringProperty getSimpleName () {return name;}
    public void setName (String newName) {name.set(newName);}
    
    public String getReturnType () {return returnType.get();}
        public SimpleStringProperty getSimpleReturn () {return returnType;}
    public void setReturnType (String newReturnType) {returnType.set(newReturnType);}
    
    public AccessType getAccess () {return access;}
    public String getAccessString () {return accessString.get();}
    public SimpleStringProperty getSimpleAccess () {return accessString;}
    public void setAccess (AccessType newAccess) {
        access = newAccess;
        accessString.set(access.getWord());
    }
    
    public boolean getIsStatic (){return isStatic.get();}
        public SimpleBooleanProperty getSimpleStatic () {return isStatic;}
    public void setIsStatic (boolean isStatic) {this.isStatic.set(isStatic);}
    
    public boolean getIsAbstract (){return isAbstract.get();}
            public SimpleBooleanProperty getSimpleAbstract () {return isAbstract;}
    public void setIsAbstract (boolean isAbstract) {this.isAbstract.set(isAbstract);}
    
    public void clearArgs() {
        args.clear();
    }
    
    public void addArg(String name, String type) {
        args.put(name, type);
    }
    
    public void removeArg(String name) {
        args.remove(name);
    }
    
    public String getArg (String name) {
        return args.get(name) +  " " + name;
    }
    
    public HashMap<String,String> getArgMap () {return args;}
    public void setArgMap (HashMap<String,String> newMap) {args = newMap;}
    
    public String toUMLString () {
        String s = access.getSymbol();
        if (getIsStatic())
            s += "$";
        
        s += getName() + "(";
        if (args.size() > 0) {
            Iterator argsIterator = args.entrySet().iterator();
            while (argsIterator.hasNext()) {
                Map.Entry arg = (Map.Entry) argsIterator.next();
                s += (arg.getKey() + ":" + arg.getValue());
                s += ", "; 
            }
            s = s.substring(0,s.lastIndexOf(','));
        }
        s += ("): " + getReturnType());
        
        if (getIsAbstract()) 
            s += " {abstract}";
        
        return s.trim();
    }
    
    public String toJavaCode() {
        String returnType = this.returnType.get();
        String s = access.getWord() + " ";
        
        if (isStatic.get())
            s += "static ";
        else if (isAbstract.get())
            s += "abstract ";
        
        s += (returnType + " " + getName() + " (");
        
        
        if (args.size() > 0) {
            Iterator argsIterator = args.entrySet().iterator();
            while (argsIterator.hasNext()) {
                Map.Entry arg = (Map.Entry) argsIterator.next();
                s += (arg.getValue() + " " + arg.getKey());
                s += ", ";
            }
            s = s.substring(0,s.lastIndexOf(','));
        }
        s += ")";
        
        if (isAbstract.get()) 
            s += ";";
        else { 
            s += "{";
            if (!returnType.equals("void") && !returnType.equals("") ) {
                s += "return ";
                if (returnType.equals("byte") || returnType.equals("short") || returnType.equals("int")
                        || returnType.equals("long") || returnType.equals("float") || returnType.equals("double")) 
                    s += "0;";
                else if (returnType.equals("boolean")) 
                    s += "false;";
                else if (returnType.equals("char")) 
                    s += ('\u0000' + ";");
                else 
                    s += "null;";
            }
            s += "}";
        }
        return s;
    }
    
    /*
    public static void main (String[] args) {
        Method m = new Method();
        m.setAccess(AccessType.PRIVATE);
        m.addArg("x", "int");
        m.addArg("y", "double");
        System.out.println(m.toUMLString());
        System.out.println(m.toJavaCode());
    }
*/
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

/**
 *
 * @author haley.he
 */
public enum jClassStates {
    SELECT,
    DRAG_UML,
    DRAG_NOTHING,
    RESIZE,
    RESIZE_UML_H,
    RESIZE_UML_V,
    RESIZE_NOTHING,
    ADD_CLASS,
    ADD_INTERFACE,
    REMOVE
}

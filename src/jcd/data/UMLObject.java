/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 *
 * @author haley.he
 */
public class UMLObject extends Group {
    public static final int TEXT_Y_OFFSET = 15;
    public static final int TEXT_X_OFFSET = 10; 
    public static final int TEXT_ADD_SPACE = 20;
    public static final int NAME_BOX_MIN_H = 70;
    public static final int NAME_BOX_MIN_W = 120;
    static final String INTERFACE_STRING = "<Interface>";
    static final String ABSTRACT_STRING = "{abstract}";
    
    int id;
    int nodeIndex;
    int parentIndex;
    int parentID;
    ArrayList<Integer> interfaceIDs;
    
    double startX;
    double startY;
    double xPos;
    double yPos;
    double w;
    double h;
    
    boolean showVars;
    boolean showMethods;
    
    boolean isAbstract;
    boolean isInterface;
    String packageName;

    Rectangle nameBox;
    Text nameText;
    String name;
    int nameHeight;
    Text typeText;
    
    Rectangle varBox;
    Text varText;
    StringBuffer varString;
    HashMap<String, Variable> varMap;
    int varHeight;
    
    Rectangle methodBox;
    Text methodText;
    StringBuffer methodString;
    HashMap<String, Method>methodMap;
    int methodHeight;
    
    public UMLObject (int id, boolean isInterface) {
        this.id = id;
        
        this.isInterface = isInterface;
        xPos = 0;
        yPos = 0;
        nameHeight = 100;
        varHeight = 0;       
        methodHeight = 0;
        w = 150;
        h = nameHeight + varHeight + methodHeight;
        
        varString = new StringBuffer();
        methodString = new StringBuffer();
        
        showVars = true; //!isInterface;
        showMethods = true; //true;
        
        isAbstract = false;
        packageName = "DefaultPackage";
        parentID = -1;
        interfaceIDs = new ArrayList<>();
        
        if (isInterface)
            name = "Interface";
        else 
            name = "Class";
        
        varMap = new HashMap<>();
        methodMap = new HashMap<>();
    }

    public void start(double x, double y) {
        startX = x;
        startY = y;
        xPos = x;
        yPos = y;
        this.relocate(xPos, yPos);
       
        nameBox = new Rectangle (w, nameHeight, Color.WHITE);
        nameBox.setStroke(Color.BLACK);
        nameText = new Text(name);
        nameText.setX(w/8);
        nameText.setY(nameHeight/2);
        nameText.setWrappingWidth((w/4) * 3);
        typeText = new Text();
        typeText.setX(w/4);
        typeText.setY(nameHeight/2 - 15);
        if (isInterface)
            typeText.setText(INTERFACE_STRING);
        else if (isAbstract)
            typeText.setText(ABSTRACT_STRING);
        
        varBox = new Rectangle(w,varHeight, Color.WHITE);
        varBox.setStroke(Color.BLACK);
        varBox.setY(nameHeight);
        
        varText = new Text();
        varText.setX(TEXT_X_OFFSET);
        varText.setY(nameHeight+TEXT_Y_OFFSET);
        varText.setWrappingWidth(w);
        
        
        methodBox = new Rectangle(w,methodHeight, Color.WHITE);
        methodBox.setStroke(Color.BLACK);
        methodBox.setY(nameHeight+varHeight);
        
        methodText = new Text();
        methodText.setX(TEXT_X_OFFSET);
        methodText.setY(nameHeight+varHeight+TEXT_Y_OFFSET);
        methodText.setWrappingWidth(w);
        this.getChildren().addAll(nameBox, varBox, methodBox, nameText, typeText, varText, methodText);
    }

    public void drag(double x, double y) {
        double diffX = x - xPos;
        double diffY = y - yPos;
        xPos += diffX - w/2;
        yPos += diffY - h/2;
        this.relocate(xPos, yPos);
        startX = xPos;
        startY = yPos;
        
    }
    
    public void drag(double x, double y, int gridWidth) {
        double diffX = x - xPos;
        double diffY = y - yPos;
        double newX = xPos + diffX - w/2;
        double newY = yPos + diffY - h/2;
        if (newX % gridWidth == 0) {
            this.relocate(newX, yPos);
            xPos = newX;;
        }
        if (newY % gridWidth == 0) {
            this.relocate(xPos, newY);
            yPos = newY;
        }
    }

    public void sizeW(double x) {
        double nameWidth = name.length()*8;
        if (nameWidth < NAME_BOX_MIN_W)
            nameWidth = NAME_BOX_MIN_W;
        
        if ((x - xPos) >= nameWidth) {
            w = x - xPos;
            nameBox.setWidth(w);
            nameText.setWrappingWidth(w);
            varBox.setWidth(w);
            varText.setWrappingWidth(w);
            methodBox.setWidth(w);
            methodText.setWrappingWidth(w);
        }
    }
    
    public void sizeH(double y) {
        if ((y - yPos) >= NAME_BOX_MIN_H)
            nameHeight = (int) (y - yPos);
    }

    public double getX() {return xPos;}
    public void setX (double x) {xPos = x;}

    public double getY() {return yPos;}
    public void setY (double y) {yPos = y;}

    public double getWidth() {return w;}
    public void setWidth (double newW) {w = newW;}

    public double getHeight() {return h;}
    public void setHeight (double newH) {h = newH;}
    
    public int getID(){return id;}
    
    public String getName () {return name;}
    public void setName (String newName) {name = newName;}
    
    public String getPackageName () {return packageName;}
    public void setPackageName (String newPackageName) {packageName = newPackageName;}
       
    public int getNameHeight () {return nameHeight;}
    public void setNameHeight (int nameH) {nameHeight = nameH;}
    
    public int getVarHeight () {return varHeight;}
    public void setVarHeight (int varH) {varHeight = varH;}
    
    public ArrayList<Integer> getInterfaceIDs() {return interfaceIDs;}
    
    public ArrayList<UMLObject> getInterfaceArray (DataManager dataM) {
        ArrayList<UMLObject> interfaces = new ArrayList<>();
        ArrayList<UMLObject> umls  = dataM.getUMLObjs();
        for (int i=0; i<interfaceIDs.size(); i++) {
            for (UMLObject uml : umls) {
                if (interfaceIDs.get(i) == uml.getID()) {
                    interfaces.add(uml);
                    break;
                }
            }
        }
        return interfaces;
    }
    
    public int getParentID() {return parentID;}
    public void setParentID(int id) {parentID = id;}
    
    public UMLObject getParentClass (DataManager dataM) {
        ArrayList<UMLObject> umls  = dataM.getUMLObjs();
        for (UMLObject uml: umls) {
            if (this.parentID == uml.getID())
                return uml;
        }
        return null;
    } 
    public void setParentClass(UMLObject parentClass) {parentID = parentClass.getID();}
        
    public int getMethodHeight () {return methodHeight;}
    public void setMethodHeight (int methodH) {methodHeight = methodH;}
    
    public int getNodeIndex () {return nodeIndex;}
    public void setNodeIndex (int newNodeIndex) {nodeIndex = newNodeIndex;}
    
    public int getParentIndex () {return parentIndex;}
    public void setParentIndex (int newParentIndex) {parentIndex = newParentIndex;}
    
    public boolean getIsInterface (){return isInterface;}
    
    public boolean getIsAbstract (){return isAbstract;}
    public void setIsAbstract (boolean isAbstract) {this.isAbstract = isAbstract;}
    
    public boolean getShowVars () {return showVars;}
    public void setShowVars (boolean show) {showVars = show;}
    
    public boolean getShowMethods () {return showMethods;}
    public void setShowMethods (boolean show) {showMethods = show;}
    
    public HashMap<String,Variable> getVarMap () {return varMap;}
    public HashMap<String,Method> getMethodMap () {return methodMap;}
    
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xPos = initX;
        yPos = initY;
        w = initWidth;
        h = nameHeight + varHeight + methodHeight;
        this.relocate(xPos, yPos);
    }
    
    public void updateUMLContent() {
         //Update the width and height of the nameBox, The x,y, coord of class box should always be 0,0
        nameBox.setHeight(nameHeight);
        nameText.setText(name);
        
        //Update variable box, x coord should always be 0
        varBox.setY(nameHeight);
        varHeight = 0;
        varString = new StringBuffer();
        double varWidth = 0;
        if (showVars) {
            Set<String> varKey = varMap.keySet();
            for (String key: varKey) {
                Variable var = varMap.get(key);
                if (var.toUMLString().length() > varWidth)
                    varWidth = var.toUMLString().length();
                varHeight += TEXT_ADD_SPACE;
                varString.append(var.toUMLString());
                varString.append('\n');
            }
        }
        varBox.setHeight(varHeight);
        varText.setText(varString.toString());
        varText.setY(nameHeight+TEXT_Y_OFFSET);
        varText.setWrappingWidth(w);
                
        methodBox.setY(nameHeight + varHeight);
        methodHeight = 0; 
        methodString = new StringBuffer();
        double methodWidth = 0;
        if (showMethods) {
            Set<String> methodKey = methodMap.keySet();
            for (String key: methodKey) {
                Method method = methodMap.get(key);
                if (method.toUMLString().length() > methodWidth)
                    methodWidth = method.toUMLString().length();
                methodHeight += TEXT_ADD_SPACE;
                methodString.append(method.toUMLString());
                methodString.append('\n');
            }
        }
        methodBox.setHeight(methodHeight); 
        methodText.setText(methodString.toString());
        methodText.setY(nameHeight+varHeight+TEXT_Y_OFFSET);
        methodText.setWrappingWidth(w);
        
        h = nameHeight + varHeight + methodHeight;
        
        double nameWidth = name.length()*8;
        varWidth = (varWidth * 8) + TEXT_X_OFFSET;
        methodWidth = (methodWidth * 8) + TEXT_X_OFFSET;
        double maxWidth = Math.max(varWidth, methodWidth);
        maxWidth = Math.max(maxWidth, nameWidth);
        if (maxWidth > w) 
            w = maxWidth;
     
        nameBox.setWidth(w);
        nameText.setWrappingWidth(w);
        varBox.setWidth(w);
        varText.setWrappingWidth(w);
        methodBox.setWidth(w);
        methodText.setWrappingWidth(w);
    }

    public boolean equals (UMLObject umlObj) {
        if (this.name.equals(umlObj.getName()) && 
                this.packageName.equals(umlObj.getPackageName()) &&
                this.xPos == umlObj.getX() &&
                this.yPos == umlObj.getY())
            return true;
        
        else 
            return false;
    }
    
    public boolean equalsName (UMLObject umlObj) {
        if (this.name.equals(umlObj.getName()) && this.packageName.equals(umlObj.getPackageName()))

            return true;
        
        else 
            return false;
    }
    
    public String exportToCode(DataManager dataM) {
        ArrayList<UMLObject> umls = dataM.getUMLObjs();
        StringBuffer code = new StringBuffer();
        if (packageName != "src") {
            code.append("package " + packageName +  ";" + '\n');
        }
        //import stuff
        if (parentID != -1) { 
            UMLObject parentClass = this.getParentClass(dataM);
            code.append("import " + parentClass.getPackageName() + "." + parentClass.getName() + ";" + '\n');
        }
        
        ArrayList<UMLObject> interfaceArray = this.getInterfaceArray(dataM);
        for (int i=0; i<interfaceArray.size(); i++) {
            UMLObject parentUML = interfaceArray.get(i);
            code.append("import " + parentUML.getPackageName() + "." + parentUML.getName() + ";" + '\n');
        }
        
        Set<String> varKeys = this.varMap.keySet();
        for (String key : varKeys) {
            Variable var = varMap.get(key);
            for (UMLObject uml : umls) {
                if (uml.getName().equals(var.getType()) && !(uml.getPackageName().equals(this.getPackageName()))) 
                    code.append("import " + uml.getPackageName() + "." + uml.getName() + ";" + '\n');
            }
        }

        Set<String> methodKeys = this.methodMap.keySet();
        for (String key : methodKeys) {
            Method method = methodMap.get(key);
            for (UMLObject uml : umls) {
                if (uml.getName().equals(method.getReturnType()) && !(uml.getPackageName().equals(this.getPackageName()))) {
                    code.append("import " + uml.getPackageName() + "." + uml.getName() + ";" + '\n');
                }
            };
            HashMap<String, String> argMap = method.getArgMap();
            Set<String> argKeys = argMap.keySet();
            for (String argKey : argKeys) {
                String argType = argMap.get(argKey);
                for (UMLObject uml : umls) { 
                    if (uml.getName().equals(argType) && !(uml.getPackageName().equals(this.getPackageName()))) 
                        code.append("import " + uml.getPackageName() + "." + uml.getName() + ";" + '\n');

                }
            }
        }
        code.append('\n');
        
        //header
        code.append("public ");
        if (isAbstract)
            code.append("abstract ");
        
        if (isInterface)
            code.append("interface " + this.name + " ");
        else 
            code.append("class " + this.name + " ");
         
        if (this.getParentID() != -1) { 
            UMLObject parentClass = this.getParentClass(dataM);
            code.append("extends " + parentClass.getName() + " ");
        }
        
        for (int i=0; i< interfaceArray.size(); i++) {
            UMLObject parentUML = interfaceArray.get(i);
            code.append("implements " + parentUML.getName() + " ");
        }
        
        code.append(" { " + '\n');
        code.append('\n');
        //var
        for (String key: varKeys) {
            Variable var = varMap.get(key);
            code.append(var.toJavaCode());
            code.append(";" + '\n');
        }
        code.append('\n');
       
        //methods
        for (String key: methodKeys) {
            Method method = methodMap.get(key);
            code.append(method.toJavaCode());
            code.append('\n');
        }
        //close
                code.append("} " + '\n');
        return code.toString();
    };
     
    public ArrayList<String> findNonPrimitiveVar () {
        ArrayList<String> nonPrims = new ArrayList<>();
        
        Set<String> varKeys = this.varMap.keySet();
        for (String key : varKeys) {
            Variable var = varMap.get(key);
            String varType = var.getType();
            if (!(varType.equals("byte") || varType.equals("short") || varType.equals("int")
                    || varType.equals("long") || varType.equals("float") || varType.equals("double")
                    || varType.equals("char") || varType.equals("String") || varType.equals("boolean")
                    || nonPrims.contains(varType))) 
                nonPrims.add(varType);    
        }
        return nonPrims;
    }
    
    public ArrayList<String> findNonPrimitiveMethod () {
        ArrayList<String> nonPrims = new ArrayList<>();
        Set<String> methodKeys = this.methodMap.keySet();
        for (String key : methodKeys) {
            Method method = methodMap.get(key);
            String returnType = method.getReturnType();
            if (!(returnType.equals("void") || returnType.equals("byte") || returnType.equals("short") || returnType.equals("int")
                    || returnType.equals("long") || returnType.equals("float") || returnType.equals("double")
                    || returnType.equals("char") || returnType.equals("String") || returnType.equals("boolean")
                    || nonPrims.contains(returnType)))
                nonPrims.add(returnType);
        }
        return nonPrims;
    }
    
    public ArrayList<String> findNonPrimitiveArg () {
        ArrayList<String> nonPrims = new ArrayList<>();   
        Set<String> methodKeys = this.methodMap.keySet();
        for (String key : methodKeys) {
            Method method = methodMap.get(key);         
            HashMap<String, String> argMap = method.getArgMap();
            Set<String> argKeys = argMap.keySet();
            for (String argKey : argKeys) {
                String argType = argMap.get(argKey);
                if (!(argType.equals("byte") || argType.equals("short") || argType.equals("int")
                        || argType.equals("long") || argType.equals("float") || argType.equals("double")
                        || argType.equals("char") || argType.equals("String") || argType.equals("boolean")
                        || nonPrims.contains(argType)))
                    nonPrims.add(argType);
            }
        }
        return nonPrims;
    }
    
}

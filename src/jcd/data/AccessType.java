/*
    This enum holds the possible access types for variables and methods
 */
package jcd.data;

/**
 *
 * @author haley.he
 */
public enum AccessType {
    PUBLIC {
        @Override
        public String getSymbol() {
            return "+";
        }
        @Override
        public String getWord() {
            return "public";
        }
    },
    PRIVATE {
        @Override
        public String getSymbol() {
            return "-";
        }
        @Override
        public String getWord() {
            return "private";
        }
    },
    PROTECTED {
        @Override
        public String getSymbol() {
            return "#";
        }
        @Override
        public String getWord() {
            return "protected";
        }
    },    
    DEFAULT {
        @Override
        public String getSymbol() {
            return "";
        }
        @Override
        public String getWord() {
            return "";
        }
        
        public String toString() {
            return "default";
        }
    };
    
    public abstract String getSymbol(); 
    public abstract String getWord();
}

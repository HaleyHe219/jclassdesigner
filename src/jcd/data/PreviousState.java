/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.scene.Node;

/**
 *
 * @author haley.he
 */
public class PreviousState {
    Variable var;
    Method method;
    UMLObject uml;
    ArrayList<Node> line;
    int zoom;
    boolean snap;
    boolean isGrid;
    
    public PreviousState () {
        zoom = 40;
        snap = false;
        isGrid = true;
    }
    
    public void updateElement (UMLObject selectedUml) {
        uml = selectedUml;
    }
    
    public void updateElement (Variable selectedVar) {
        var = selectedVar;
    }
    
    public void updateElement (Method selectedMethod) {
        
    }
}
    
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

/**
 *
 * @author haley.he
 */
public class JavaPackage {
    String packageName;
    //int numChildren;
    int nodeIndex;
    int parentIndex;
    
    public JavaPackage (String name) {
        packageName = name;
    }
    
    public String getPackageName () {return packageName;}
    
    //public int getNumChildren () {return numChildren;}
    //public void setNumChildren (int numChildren) {this.numChildren = numChildren;}
    
    public int getNodeIndex () {return nodeIndex;}
    public void setNodeIndex (int newNodeIndex) {nodeIndex = newNodeIndex;}
    
    public int getParentIndex () {return parentIndex;}
    public void setParentIndex (int newParentIndex) {parentIndex = newParentIndex;}
}

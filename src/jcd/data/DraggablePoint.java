/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import javafx.scene.shape.Circle;

/**
 *
 * @author haley.he
 */
public class DraggablePoint extends Circle {
    double startX;
    double startY;
    
    public DraggablePoint(double x, double y) {
        startX = x;
        startY = y;
        setCenterX(x);
        setCenterY(y);
        this.setRadius(3);
    }

    
    public void start(int x, int y) {
    }

    //@Override
    public void drag(int x, int y) {
        double diffX = x - startX;
	double diffY = y - startY;
	double newX = getCenterX() + diffX;
	double newY = getCenterY() + diffY;
	setCenterX(newX);
	setCenterY(newY);
	startX = x;
	startY = y;
    }

    //@Override
    public void setLocation(double initX, double initY, double initWidth, double initHeight) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

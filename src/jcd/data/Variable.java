/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import jcd.data.AccessType;


/**
 *
 * @author haley.he
 */
public class Variable {
    SimpleStringProperty name;
    SimpleStringProperty type;
    SimpleStringProperty accessString;
    AccessType access;
    SimpleBooleanProperty isStatic;
    
    public Variable() {
        name = new SimpleStringProperty ("varName");
        type = new SimpleStringProperty ("String");
        access = AccessType.DEFAULT;
        if (access != AccessType.DEFAULT)
            accessString = new SimpleStringProperty(access.getWord());
        else 
            accessString = new SimpleStringProperty("default");
        isStatic = new SimpleBooleanProperty(false);
    }
    
    public Variable (String name, String type, String access, Boolean isStatic) {
        this.name = new SimpleStringProperty (name);
        this.type = new SimpleStringProperty (type);
        this.isStatic = new SimpleBooleanProperty (isStatic);
        if (access.equals("public")) 
            this.access = AccessType.PUBLIC;
        else if (access.equals("private"))
            this.access = AccessType.PRIVATE;
        else if (access.equals("protected"))
            this.access = AccessType.PROTECTED;
        else 
            this.access = AccessType.DEFAULT;
        
        
          if (this.access != AccessType.DEFAULT)
            accessString = new SimpleStringProperty(this.access.getWord());
        else 
            accessString = new SimpleStringProperty("default");
    }
    
    public String getName () {return name.get();}  
    public SimpleStringProperty getSimpleName () {return name;}
    public void setName (String newName) {name.set(newName);}
    
    public String getType () {return type.get();}
    public SimpleStringProperty getSimpleType () {return type;}
    public void setType (String newType) {type.set(newType);}
    
    public AccessType getAccess () {return access;}
    public void setAccess (AccessType newAccess) {access = newAccess;}
    
    public boolean getIsStatic (){return isStatic.get();}
    public SimpleBooleanProperty getSimpleStatic () {return isStatic;}
    public void setIsStatic (boolean isStatic) {this.isStatic.set(isStatic);}
    
    public String getAccessString () {return accessString.get();}
    public SimpleStringProperty getSimpleAccess () {return accessString;}    
    public void setAccessString (String newString) {accessString.set(newString);}
    
    public String toUMLString () {
        if (getIsStatic())
            return access.getSymbol() + "$" + getName() +  ": " + getType();
        else
            return access.getSymbol() + getName() +  ": " + getType();
    }
    
    
    public String toJavaCode () {
        String s;
        if (getIsStatic())
            s = access.getWord() + " static " + getType() + " " + getName();
        else 
            s = access.getWord() + " " + getType() + " " + getName();
        return s.trim();
    }
    
}

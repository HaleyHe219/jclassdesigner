/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import jcd.gui.Workspace;
import jcd.test_bed.TestLoad;
import saf.AppTemplate;
import saf.components.AppDataComponent;

/**
 *
 * @author haley.he
 */
public class DataManager implements AppDataComponent{
    AppTemplate app;
    
    jClassStates currentState;
    
    TreeItem project;
    ArrayList<UMLObject> umlObjs;
    HashMap<String,UMLObject> umlMap;
    
    HashMap <String,TreeItem> packageMap;
    ArrayList<ObservableList<Node>> relationships;
    
    int umlCounter;
    UMLObject newUML;
    TreeItem selectedItem;
    UMLObject selectedUML;
    
    Effect highlightedEffect;
    
    int gridWidth; 
    boolean showGrid;
    boolean enforceSnap;
    
            
    public DataManager(AppTemplate initApp) {
        app = initApp;
        gridWidth = 40;
        showGrid = true;
        enforceSnap = false;
        
        umlCounter = 1;
        //initialize the root of the tree
        project = new TreeItem();
        project.setValue(new JavaPackage("src"));
        packageMap = new HashMap<>();
        umlObjs = new ArrayList<>();
        umlMap = new HashMap<>();
        relationships = new ArrayList();
    }
    
     public DataManager() {
        gridWidth = 40;
        showGrid = true;
        enforceSnap = false;
        
                umlCounter = 0;
        //initialize the root of the tree
        project = new TreeItem();
        project.setValue(new JavaPackage("src"));
        packageMap = new HashMap<>();
        umlObjs = new ArrayList<>();
        umlMap= new HashMap<>();
        relationships = new ArrayList();
    }
    
    @Override
    public void reset() {
        currentState = jClassStates.SELECT;
        
        //clear all children
        umlObjs.clear();
        umlMap.clear();
        relationships.clear();
        project.getChildren().clear();
        packageMap.clear();
        packageMap.put("src", project);
        //add a default package
        JavaPackage defPackage = new JavaPackage ("DefaultPackage");
        TreeItem defPackageItem = new TreeItem(defPackage);
        packageMap.put(defPackage.getPackageName().toLowerCase(), defPackageItem);
        project.getChildren().add(defPackageItem);
        
        //add package for java imports
        JavaPackage javaPackage = new JavaPackage("java");
        TreeItem javaPackageItem = new TreeItem (javaPackage);
        packageMap.put(javaPackage.getPackageName().toLowerCase(), javaPackageItem);
        project.getChildren().add(javaPackageItem);
        
        //add package for javafx imports
        JavaPackage javafxPackage = new JavaPackage("javafx");
        TreeItem javafxPackageItem = new TreeItem (javafxPackage);
        packageMap.put(javafxPackage.getPackageName().toLowerCase(), javafxPackageItem);
        project.getChildren().add(javafxPackageItem);
        
        // THIS IS FOR THE SELECTED SHAPE
	DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(Color.YELLOW);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(15);
	highlightedEffect = dropShadowEffect;
    }
      
    public void addClassUML (double x, double y) {      
        UMLObject newClassObj = new UMLObject(umlCounter, false);
        umlCounter++;
        newClassObj.start(x, y);
        newUML = newClassObj;
        initNewUML();
    }
    
    public void addInterfaceUML (double x, double y) {
        UMLObject newClassObj = new UMLObject(umlCounter, true);
        umlCounter++;
        newClassObj.start(x, y);
        newUML = newClassObj;
        initNewUML();
    }
    
    public void removeUML() {
        if (selectedUML != null) {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            //remove item from tree
            TreeItem parentPackage = selectedItem.getParent();
            parentPackage.getChildren().remove(selectedItem);

            //remove from data
            umlObjs.remove(selectedUML);
            umlMap.remove(selectedUML.getName(), selectedUML);
            int x = (int) selectedUML.getX();
            int y = (int) selectedUML.getY();
            for (int i = 0; i < relationships.size(); i++) {
                ObservableList<Node> relation = relationships.get(i);
                for (int j = 0; j < relation.size(); j++) {
                    Node node = relation.get(j);
                    if (node instanceof Line) {
                        Line l = (Line) node;
                        int lStartX = (int) l.getStartX();
                        int lStartY = (int) l.getStartY();
                        int lEndX = (int) l.getEndX();
                        int lEndY = (int) l.getEndY();
                        
                        if ((lStartX == x && lStartY == y) ||(lEndX == x && lEndY == y)) {
                            relationships.remove(i);
                            workspace.getCanvas().getChildren().removeAll(relation);
                            break;
                        }
                    }
                }
            }
            workspace.getCanvas().getChildren().remove(selectedUML);
            workspace.loadNoSelectedUMLSetting();
            selectedUML = null;
            selectedItem = null;
            app.getGUI().updateToolbarControls(false);
        }
    }
    
    public void removeConnectingRelation(UMLObject uml) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        int x = (int) uml.getX();
        int y = (int) uml.getY();

        for (int i = 0; i < relationships.size(); i++) {
            ObservableList<Node> relation = relationships.get(i);
            for (int j = 0; j < relation.size(); j++) {
                Node node = relation.get(j);
                if (node instanceof Line) {
                    Line l = (Line) node;
                        int lStartX = (int) l.getStartX();
                        int lStartY = (int) l.getStartY();
                        int lEndX = (int) l.getEndX();
                        int lEndY = (int) l.getEndY();
                    if ((lStartX == x && lStartY == y) ||(lEndX == x && lEndY == y)) {
                        relationships.remove(i);
                        workspace.getCanvas().getChildren().removeAll(relation);
                        break;
                    }
                }
            }
        }
    }
    
    public void initNewUML () {
        if (selectedUML != null) {
            unhighlightUML(selectedUML);
            selectedUML = null;
            selectedItem = null;
        }
        //add to data
        umlObjs.add(newUML);
        umlMap.put(newUML.getName(), newUML);
        
        //add to the UML to the default package
        TreeItem UMLItem = new TreeItem(newUML);
        TreeItem defPackage = (TreeItem) project.getChildren().get(0);
        defPackage.getChildren().add(UMLItem);
        
        selectedUML = newUML; // select new object 
        selectedItem = UMLItem;       
        highlightUML(newUML); //highlight new object
        
        //load the selectedUML's setting into the component toolbar
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getCanvas().getChildren().add(newUML);
	workspace.loadSelectedUMLSettings(newUML);
    }
    
    public UMLObject selectTopUMLObj (double x, double y) {
        UMLObject umlObj = getTopUML(x,y);
        //if the selected is alreadt the umlObj, do nothing
        if (umlObj == selectedUML)
            return umlObj;
        
       //else deselect/unhighlight the old UMLObject
        if (selectedUML != null)
            unhighlightUML(selectedUML);
        
        //highlight the new selected UMLObject
        if (umlObj != null) {
            setSelectedItem(umlObj);
            highlightUML(umlObj);
	    Workspace workspace = (Workspace)app.getWorkspaceComponent();
	    workspace.loadSelectedUMLSettings(umlObj);
        }
        selectedUML = umlObj;
        
        return umlObj;
    }
    
    private void setSelectedItem(UMLObject umlObj) {
        String packageName = umlObj.getPackageName().toLowerCase();
        if (packageName.contains("javafx")) {
            packageName = "javafx";
        } else if (packageName.contains("java")) {
            packageName = "java";
        }
        TreeItem parentPackage = packageMap.get(packageName);
        ObservableList children = parentPackage.getChildren();
        for (int i = 0; i < children.size(); i++) {
            TreeItem child = (TreeItem) children.get(i);
            if (child.getValue() instanceof UMLObject) {
                UMLObject childUML = (UMLObject) child.getValue();
                if (childUML.equals(umlObj)) {
                    selectedItem = child;
                    return;
                }
            }
        }
    }
    
    public UMLObject getTopUML (double x, double y) {
        for (int i = umlObjs.size() - 1; i >= 0; i--) { 
            if  (umlObjs.get(i)instanceof UMLObject) {
                UMLObject umlObj = (UMLObject) umlObjs.get(i);
                if (containsPt(umlObj, x, y)) 
                    return umlObj;
                }
	}
        return null;
    }
    
    private boolean containsPt (UMLObject uml, double x, double y) {       
        int xMin = (int) uml.getX();
        int yMin = (int) uml.getY();
        int xMax = (int) (uml.getX() + uml.getWidth());
        int yMax = (int) (uml.getY() + uml.getHeight());
        if (x >= xMin && x <= xMax && y >= yMin && y <= yMax) 
            return true;
        return false;
    }
    
    public UMLObject getSelectedUML () {
        return selectedUML;
    }
    
    public TreeItem getSelectedItem () {
        return selectedItem;
    }
    
    public void highlightUML (UMLObject selected) {
        selected.setEffect(highlightedEffect);
    }
    
    public void unhighlightUML (UMLObject selected) {
        selected.setEffect(null);
    }
    
    public boolean searchPackagesForClass (TreeItem root, String [] packageParse, String umlName, int nestCounter) {
        String packageName = "";
        for (int i=0; i<=nestCounter; i++) {
            packageName += packageParse[nestCounter];
            if (i+1 <= nestCounter)
                packageName += ".";
        }
        ObservableList children = root.getChildren();
        for (int i=0; i<children.size(); i++) {
            TreeItem child = (TreeItem) children.get(i);
            JavaPackage childPackage = (JavaPackage) child.getValue();
            if (nestCounter == (packageParse.length - 1) && childPackage.getPackageName().equals(packageName)) 
                   return findClass(child, umlName);       
            else if (childPackage.getPackageName().equals(packageName)) 
                    return searchPackagesForClass(child, packageParse, umlName, nestCounter+1);     
            }
        return false;
    }
    
    public boolean searchPackagesForClass2 (String packageName, String umlName) {
        packageName = packageName.toLowerCase();
        if (packageName.contains("javafx"))
            packageName = "javafx";
        else if (packageName.contains("java")) 
            packageName = "java";
        TreeItem packageToSearch = packageMap.get(packageName);
        return findClass(packageToSearch, umlName);
    }
    
    private boolean findClass(TreeItem packageToSearch, String umlName) {
        ObservableList children = packageToSearch.getChildren();
        for (int i=0; i<children.size(); i++) {
            TreeItem child = (TreeItem) children.get(i);
            if (child.getValue() instanceof UMLObject) {
                UMLObject umlObj = (UMLObject) child.getValue();
                if (umlObj.getName().equalsIgnoreCase(umlName))
                    return true;
            }
        }
        return false;
    }
    
    public void addPackage (String packageName) {
        String orginalName = packageName;
        packageName = packageName.toLowerCase();
        if (packageName.contains("javafx"))
            packageName = "javafx";
        else if (packageName.contains("java")) 
            packageName = "java";
        
        if (packageMap.containsKey(packageName))
            return; 
        else {
            //add the new package to the tree
            TreeItem newPackage = new TreeItem(new JavaPackage(orginalName));
            
            //check to see if it is a nested package
            if (packageName.contains(".")) {
                String parentPackageName = packageName.substring(0, packageName.lastIndexOf("."));
                //find parent package
                TreeItem parentPackage = packageMap.get(parentPackageName);
                //add newPackage to the Tree
                parentPackage.getChildren().add(newPackage);
            }
            else 
               project.getChildren().add(newPackage);
            
            //add the new packageMap to the hashmap
            packageMap.put(packageName, newPackage);
        }             
    }
    
    public void removeUMLFromPackage (String packageName, UMLObject umlToRemove) {
        packageName = packageName.toLowerCase();
        if (packageName.contains("javafx"))
            packageName = "javafx";
        else if (packageName.contains("java")) 
            packageName = "java";
        TreeItem oldPackage = packageMap.get(packageName);
        ObservableList children = oldPackage.getChildren();
        //System.out.println("Before Remove: " + oldPackage.getValue() + " " + children.size());
        for (int i=0; i<children.size(); i++) {
            TreeItem child = (TreeItem) children.get(i);
            if (child.getValue() instanceof UMLObject) {
                UMLObject umlObj = (UMLObject) child.getValue();
                if (umlObj.equals(umlToRemove)) {
                    oldPackage.getChildren().remove(i);
                    //System.out.println("After Remove: "  + oldPackage.getValue() + " " +  children.size());
                    return;
                }
            }
        }
    }
    
    public void addUMLToPackage (String packageName, UMLObject umlToAdd) {
        //get the new package
        packageName = packageName.toLowerCase();
                if (packageName.contains("javafx"))
            packageName = "javafx";
        else if (packageName.contains("java")) 
            packageName = "java";
        TreeItem newPackage = packageMap.get(packageName);
        //System.out.println("Before Add: "  + newPackage.getValue() + " " +  newPackage.getChildren().size());
        //create a TreeItem for the umlToAdd and add it to the new package
        TreeItem itemToAdd = new TreeItem(umlToAdd);
        newPackage.getChildren().add(itemToAdd);
        //System.out.println("After Add: " + newPackage.getValue() + " " + newPackage.getChildren().size());
    }
    
    public jClassStates getCurrentState() {return currentState;}
    public void setCurrentState(jClassStates newState) {currentState = newState;} 
    public boolean isInState(jClassStates state) {
        if (currentState==state)
            return true;
        else
            return false;
                  
    }
    /*
    public void setUMLObjs(ObservableList<Node> initUMLObjs) {
	umlObjs = initUMLObjs;
    }
    */
    public ArrayList<UMLObject> getUMLObjs () {return umlObjs;}
    public HashMap<String,UMLObject> getUMLmap () {return umlMap;}
    
    public int getGridWidth() {return gridWidth;}
    public void setGridWidth(int newZoom) {gridWidth =  newZoom;}
    
    public boolean getShowGrid () {return showGrid;}
    public void setShowGrid (boolean showZoom) {this.showGrid = showZoom;}
    
    public int getUMLCounter () {return umlCounter;}
    public void setUMLCounter (int nextID) {umlCounter = nextID;}
    
    public boolean getEnforceSnap () {return enforceSnap;}
    public void setEnforceSnap  (boolean enforceSnap) {this.enforceSnap = enforceSnap;}
    
    public TreeItem getProject() {return project;}
    public void setProject (TreeItem newProj) {project = newProj;}
    
    public HashMap<String,TreeItem> getPackageMap () {return packageMap;}
    public ArrayList<ObservableList<Node>> getRelationships () {return relationships;}
    public void setRelationships (ArrayList<ObservableList<Node>> newRelations) {relationships = newRelations;}
    
    public void createAPIClass (String name) {
        UMLObject newClassObj = new UMLObject(umlCounter, false);
        umlCounter++;
        double x = Math.random() * 100 + 10;
        double y = Math.random() * 100 + 10;
        newClassObj.start(x, y);
        newClassObj.setName(name);
        newClassObj.updateUMLContent();
        
        //add to data
        umlObjs.add(newClassObj);
        umlMap.put(newClassObj.getName(), newClassObj);
        
        //add to the UML to the default package
        TreeItem UMLItem = new TreeItem(newClassObj);
        TreeItem defPackage = (TreeItem) project.getChildren().get(0);
        defPackage.getChildren().add(UMLItem);
        
        //load the selectedUML's setting into the component toolbar
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getCanvas().getChildren().add(newClassObj);
    }
    
    public void addAPIs (UMLObject uml, String type) {
        ArrayList<String> addUMLNames;
        switch (type) {
            case "var":
                addUMLNames = uml.findNonPrimitiveVar();
                break;
            case "method":
                addUMLNames = uml.findNonPrimitiveMethod();
                break;
            case "arg":
                addUMLNames = uml.findNonPrimitiveArg();
                break;
            default:
                return;
        }
        
        for (String s : addUMLNames)
            if (!umlMap.containsKey(s))
            createAPIClass(s);
    }
    
    public void generateParentLines(UMLObject uml) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        UMLObject parent = uml.getParentClass(this);
        if (parent != null) {
            Line parentLine = new Line();
            parentLine.startXProperty().bind(uml.layoutXProperty()); //.add(uml.translateXProperty()));
            parentLine.startYProperty().bind(uml.layoutYProperty()); //.add(uml.translateYProperty()));
            parentLine.endXProperty().bind(parent.layoutXProperty());//.add(parent.translateXProperty()));
            parentLine.endYProperty().bind(parent.layoutYProperty()); //.add(parent.translateYProperty()));
            
            ObservableList<Node> parentRelation = FXCollections.observableArrayList();
            parentRelation.add(parentLine);
            workspace.getCanvas().getChildren().addAll(parentRelation);
            relationships.add(parentRelation);
        }
    }

    public void generateInterfaceLines(UMLObject uml) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ArrayList<UMLObject> interfaceParents = uml.getInterfaceArray(this);
        for (UMLObject iParent : interfaceParents) {
            Line interfaceLine = new Line();
            interfaceLine.startXProperty().bind(uml.layoutXProperty()); 
            interfaceLine.startYProperty().bind(uml.layoutYProperty()); 
            interfaceLine.endXProperty().bind(iParent.layoutXProperty()); 
            interfaceLine.endYProperty().bind(iParent.layoutYProperty()); 
            interfaceLine.getStrokeDashArray().addAll(10d);
            ObservableList<Node> interfaceRelation = FXCollections.observableArrayList();
            interfaceRelation.add(interfaceLine);
            workspace.getCanvas().getChildren().addAll(interfaceRelation);
            relationships.add(interfaceRelation);
        }
    }

    public void generateAggregateLines(UMLObject uml) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ArrayList<String> varEndptNames = uml.findNonPrimitiveVar();
        for (String name : varEndptNames) {
            UMLObject aggregate = umlMap.get(name);
            if (aggregate != null) {
                Line aggregateLine = new Line();
                aggregateLine.startXProperty().bind(uml.layoutXProperty());
                aggregateLine.startYProperty().bind(uml.layoutYProperty());
                aggregateLine.endXProperty().bind(aggregate.layoutXProperty());
                aggregateLine.endYProperty().bind(aggregate.layoutYProperty());

                Rectangle end = new Rectangle(10, 10);
                end.xProperty().bind(uml.layoutXProperty().subtract(5));
                end.yProperty().bind(uml.layoutYProperty().subtract(5));
                end.setFill(Color.WHITE);
                end.setStroke(Color.BLACK);
                end.rotateProperty().set(45);

                ObservableList<Node> aggRelation = FXCollections.observableArrayList();
                aggRelation.addAll(aggregateLine, end);
                workspace.getCanvas().getChildren().addAll(aggRelation);
                relationships.add(aggRelation);
            }
        }
    }
    
    public void generateMethAssociationLines(UMLObject uml) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ArrayList<String> methodEndptNames = uml.findNonPrimitiveMethod();
        for (String name : methodEndptNames) {
            UMLObject association = umlMap.get(name);
            if (association != null) {
                Line associationLine = new Line();
                associationLine.startXProperty().bind(uml.layoutXProperty());
                associationLine.startYProperty().bind(uml.layoutYProperty());
                associationLine.endXProperty().bind(association.layoutXProperty());
                associationLine.endYProperty().bind(association.layoutYProperty());
                
                ObservableList<Node> assocRelation = FXCollections.observableArrayList();
                assocRelation.add(associationLine);
                workspace.getCanvas().getChildren().addAll(assocRelation);
                relationships.add(assocRelation);
            }
        }
    }

    public void generateArgAssociationLines(UMLObject uml) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        ArrayList<String> argsEndptNames = uml.findNonPrimitiveArg();
        for (String name : argsEndptNames) {
            UMLObject association = umlMap.get(name);
            if (association != null) {
                Line associationLine = new Line();
                associationLine.startXProperty().bind(uml.layoutXProperty());
                associationLine.startYProperty().bind(uml.layoutYProperty());
                associationLine.endXProperty().bind(association.layoutXProperty());
                associationLine.endYProperty().bind(association.layoutYProperty());
                ObservableList<Node> assocRelation = FXCollections.observableArrayList();
                assocRelation.add(associationLine);
                workspace.getCanvas().getChildren().addAll(assocRelation);
                relationships.add(assocRelation);
            }
        }
    }

    
}
